// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAaTLRU9afDW8dr4lVd1IuKevwedcFcpnw',
    authDomain: 'planocnpj.firebaseapp.com',
    databaseURL: 'https://planocnpj.firebaseio.com',
    projectId: 'planocnpj',
    storageBucket: 'planocnpj.appspot.com',
    messagingSenderId: '706478159241'
  },

  // apiUrl: 'http://192.168.0.12:3000/',
  apiUrl: 'http://localhost:3000/',
  // apiUrlImage: 'http://104.248.122.233:3000/',
  apiUrlImage: 'http://localhost:3000/photoperfil/',
  // apiUrlImage: 'http://192.168.0.12:3000/',
  apiUrlIcon: 'http://localhost:4200/assets/',
   apiUrlDocuments: 'http://104.248.122.233:3000/documents/',

  KeySecurity: '2b4fb44f-a284-4529-a37d-4ad6c8a24ce7'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
/*token api Receita web service
8da37fbe1d38debaf38077ebe11babcc6003b92915eb9040f5d308e1b987e062*/
