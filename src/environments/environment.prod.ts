export const environment = {
  production: true,
  firebase: {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: ''
  },
  // apiUrl: 'http://104.248.122.233:3000/',
  // apiUrlIcon: 'http://104.248.122.233/vendedor/assets/',
  // apiUrlImage: 'http://104.248.122.233:3000/photoperfil/',
  // apiUrlDocuments: 'http://104.248.122.233:3000/documents/',

  apiUrl: 'http://localhost:3000/',
  apiUrlImage: 'http://localhost:3000/photoperfil/',
  apiUrlIcon: 'http://localhost:4200/assets/',
  apiUrlDocuments: 'http://localhost/documents/',



  KeySecurity: '2b4fb44f-a284-4529-a37d-4ad6c8a24ce7'
};
