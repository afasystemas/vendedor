import {Person} from './Person';
import {Company} from './Company';
import {Phone} from './Phone';

export class PhoneContacts {

  id: number;
  person: Person;
  company: Company;
  phone: Phone;

  constructor() {
    this.id = 0;
    this.person = new Person();
    this.company = new Company();
    this.phone = new Phone();

  }
}
