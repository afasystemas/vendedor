import {State} from './State';

export class City {

  id: number;
  name: string;
  state: State;
  constructor() {
    // this.id = 0;
    this.state = new State();
  }
}
