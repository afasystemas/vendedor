import {Plan} from './Plan';
import {Service} from './Service';
import {Package} from './Package';
import {Demand} from './Demand';
import {TitlePlan} from './TitlePlan';
import {DemandPhone} from './DemandPhone';


export class ServiceDemandPhone {

  id: number;
  demandPhone: DemandPhone;
  titleplan: TitlePlan;

  constructor() {
    this.id = 0;
    this.demandPhone = new DemandPhone();
    this.titleplan = new TitlePlan();
  }
}
