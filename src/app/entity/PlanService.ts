import {Service} from './Service';
import {TitlePlan} from './TitlePlan';

export class PlanService {

  id: number;
  service: Service;
  titleplan: TitlePlan;
  value: number;
  description: string;
  constructor() {
    this.id = 0;
    this.service = new Service();
    this.titleplan = new TitlePlan();
    this.value = 0;
    this.description = null;
  }
}
