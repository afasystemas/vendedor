import {Demand} from './Demand';
import {TypeSale} from './TypeSale';

export class DemandTypeSale {

  id: number;
  typeSale: TypeSale;
  typesaleid: number;
  // demand: Demand;
  demandid: number;

  constructor() {
    this.id = 0;
    // this.demand = new Demand();
    this.typeSale = new TypeSale();
  }
}
