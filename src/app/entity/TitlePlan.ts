import {Plan} from './Plan';
import {PlanService} from './PlanService';
import {PlanPackage} from './PlanPackage';
import {Service} from './Service';
import {Package} from './Package';

export class TitlePlan {

  id: number;
  title: string;
  valueTotal: number;
  plan: Plan;
  additional: boolean;
  listServices: Array<Service>;
  listPackages: Array<Package>;

  constructor() {
    this.id = 0;
    this.valueTotal = 0;
    this.listServices = new Array<Service>();
    this.listPackages = new Array<Package>();
  }
}
