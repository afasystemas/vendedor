import {CEP} from './CEP';

export class Address {

  id: number;
  numero: number;
  complement: string;
  latitude: number;
  longitude: number;
  cep: CEP;
  constructor() {
    this.id = 0;
    this.cep = new CEP();
  }
}
