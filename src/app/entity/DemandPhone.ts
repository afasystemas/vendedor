import {Demand} from './Demand';
import {Package} from './Package';
import {TypeSale} from './TypeSale';
import {Phone} from './Phone';
import {PlanPackage} from './PlanPackage';
import {DemandProduct} from './DemandProduct';
import {TitlePlan} from './TitlePlan';
import {PlanService} from './PlanService';
import {Plan} from './Plan';
import {ServiceDemandPhone} from './ServiceDemandPhone';

export class DemandPhone {

  id: number;
  phone: Phone;
  typeSale: TypeSale;
  demand: Demand;
  products: DemandProduct;
  totalValue: number;
  listserviceDemandPhone: Array<ServiceDemandPhone>;

  constructor() {
    this.id = 0;
    this.phone = new Phone();
    this.demand = new Demand();
    this.typeSale = new TypeSale();
    this.products = new DemandProduct();
    this.totalValue = 0;
    this.listserviceDemandPhone = Array<ServiceDemandPhone>();
  }
}
