import {DatePipe} from '@angular/common';
import {Salesman} from './Salesman';
import {StatusSale} from './StatusSale';
import {Demand} from './Demand';


export class SalesHistoric {
  id: number;
  statusSale: StatusSale;
  demand: Demand;
  initialDate: DatePipe;
  finalDate: DatePipe;
  salesman: Salesman;
  constructor() {
    this.id = 0 ;
  }
}
