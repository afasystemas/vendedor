import {Company} from './Company';
import {OrderStatus} from './OrderStatus';

export class StatusSale {

   id: number;
   name: string;
   description: string;
  constructor() {
    this.id = 0;
  }
}
