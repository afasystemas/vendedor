export class Product {

  id: number;
  name: string;
  description: string;
  value: number;
  constructor() {
    this.id = 0;
  }
  setttingProduct(id: number, name: string, desc: string, val: number) {
    this.id = id;
    this.name = name;
    this.description = desc;
    this.value = val;
  }
}
