import {Demand} from './Demand';
import {Product} from './Product';

export class DemandProduct {

  id: number;
  demand: Demand;
  product: Product;
  qtd: number;
  total: number;
  deduction: number;
  constructor() {
    this.id = 0;
    this.total = 0;
    this.deduction = 0;
    this.qtd = 1;
    this.demand = new Demand();
    this.product = new Product();
  }
}
