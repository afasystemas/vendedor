import {Package} from './Package';
import {Plan} from './Plan';

export class PlanPackage {

  id: number;
  name: string;
  Package: Package;
  plan: Plan;
  value: number;
  description: string;
  constructor() {
    this.id = 0;
    this.name = null;
    this.Package = new Package();
    this.plan =  new Plan();
    this.value = 0;
    this.description = null;
  }
}
