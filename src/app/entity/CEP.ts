import {City} from './City';
import {Neighborhood} from './Neighborhood';
import {Street} from './Street';

export class CEP {

  id: number;
  cep: number;
  neighborhood: Neighborhood;
  street: Street;
  city: City;
  constructor() {
    // this.id = 0;
    this.neighborhood = new Neighborhood();
    this.street = new Street();
    this.city = new City();

  }
}
