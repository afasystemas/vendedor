import {Address} from './Address';

export class Company {

  id: number;
  razao_social: string;
  cnpj: string;
  inscr_estadual: string;
  fantasia: string;
  inscr_municipal: string;
  cnae_primario: string;
  cnaeprimario_descricao: string;
  cnae_secundario: string;
  cnaesecundario_descricao: string;
  email: string;
  qtd_employee: number;
  address: Address;
  constructor() {
    this.id = 0;
    this.address = new Address();
  }
}
