import {Company} from './Company';
import {OrderStatus} from './OrderStatus';
import {StatusSale} from './StatusSale';
import {TypeSale} from './TypeSale';
import {AuthorizedAgent} from './AuthorizedAgent';
import {Salesman} from './Salesman';
import {Plan} from './Plan';

export class Demand {

  id: number;
  company: Company;
  plan: Plan
  orderStatus: OrderStatus;
  statusSale: StatusSale;
  fromOperator: number;
  date: Date;
  value: number;
  valueFromOperator: number;
  code: string;
  agent: AuthorizedAgent;
  salesman: Salesman;
  // typesSale: Array<TypeSale>;

  constructor() {
    this.id = 0;
    this.company = new Company();
    this.orderStatus = new OrderStatus();
    this.statusSale =  new StatusSale();
    // this.typesSale = new Array<TypeSale>();
    this.agent = new AuthorizedAgent();
    this.salesman = new Salesman();
    this.plan = new Plan();
    this.value = 0;


  }
}
/*
(Company)
 ok-nome empresa
 //abertura
 ok-representante
 ok-CNPJ
 ok-Consultor(Vendedor)
 Administrador 1(cpf telefone celular email)
 Administrador 2(cpf telefone celular email)
 Cidade da empresa
 UF
 endereço
    CEP
    bairro
    Complemento
 tel
 email
 view-Situação
 view-Porte
 ok-razão social
 ok-CNAE primario
 ok-CNAE secundario
 ok-Inscrição Estadual
 ok-Numero de funcionario
 Operadora atual
 Cliente Claro(sim, nao)
 Pontos

-contato
    admin da conta
    responsavel por aparelhor
    representante legal
    nome
    email
    tel
    cel
    rg
    cpf

-transferencia de titularidade
    nome ou razao social
    cep
    logradouro
    numero
    bairro
    complemento
    cidade
    uf
    cpf ou cnpj
    tel


 tipos de vendas
 plano
 total de linhas
 detales de cada linhas (numero, tipo de venda, pacote do plano, DDD )
 produtos detalhados (qtd de cada, valor, desconto, total) total de valor dos produtos
 serviços
 tempo de Vigência do contrato
 data de vencimento do contrato
 dia do mes para vencimento da conta para pagamento
 Forma de Pagamento

 VAlor total de Serviços
 VAlor total de Aparelhos
 Valor Total da VEnda

 */
