import {Phone} from './Phone';
import {Company} from './Company';

export class CompanyPhone{

  id: number;
  Phone: Phone;
  Company: Company;
  constructor() {
    this.id = 0;
    this.Company = new Company();
    this.Phone = new Phone();
  }
}
