import {DDD} from './DDD';

export class Phone {

  id: number;
  numero: number;
  ddd: DDD;
  constructor() {
    this.id = 0;
    this.ddd = new DDD();
  }
}
