import {TitlePlan} from './TitlePlan';

export class Plan {

  id: number;
  name: string;
  valueTotal: number;
  subtitles: Array<TitlePlan>;
  constructor() {
    this.id = 0;
    // this.subtitles = new Array<TitlePlan>();
    this.subtitles = [];
  }
}
