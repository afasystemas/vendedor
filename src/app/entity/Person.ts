import {Responsibility} from './Responsibility';
import {Company} from './Company';
import {Phone} from './Phone';

export class Person {

  id: number;
  name: string;
  email: string;
  phone: Phone;
  responsibility: Responsibility;
  company: Company;

  constructor() {
    this.id = 0;
    this.phone = new Phone();
    this.responsibility = new Responsibility();
  }
}
