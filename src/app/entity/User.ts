import {AuthorizedAgent} from './AuthorizedAgent';

export class User {

  id: number;
  name: string;
  email: string;
  password: string;
  authorizedAgent: AuthorizedAgent;
  urlphotoprofile: string;
  constructor() {
    this.id = 0;
  }

}
