import {DatePipe} from '@angular/common';
import {SalesHistoric} from './SalesHistoric';

export class Activity {

  id: number;
  title: string;
  description: string;
  timeDate: DatePipe;
  saleHistoric: number;
  SaleHistoric: SalesHistoric;
  constructor() {
  }
}
