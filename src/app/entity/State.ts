export class State {

  id: number;
  name: string;
  uf: string;
  constructor() {
    this.id = 0;
    this.name = '';
    this.uf = '';
  }
}
