import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {routes} from './app.routes';
import {RouterModule} from '@angular/router';
// import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {GoogleChartsModule} from 'angular-google-charts';
import {DragulaModule} from 'ng2-dragula';
import { NgxLoadingModule } from 'ngx-loading';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {environment} from '../environments/environment.prod';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoggedComponent } from './front-end/logged/logged.component';
import { MenuComponent } from './front-end/menu/menu.component';
import { HeadProfileComponent } from './front-end/head-profile/head-profile.component';
import { DashboardComponent } from './front-end/dashboard/dashboard.component';
import { AccountComponent } from './front-end/account/account.component';
import { MyDatasComponent } from './front-end/my-datas/my-datas.component';
import { BankAccountComponent } from './front-end/bank-account/bank-account.component';
import { MyDocumentsComponent } from './front-end/my-documents/my-documents.component';
import { NewOrderComponent } from './front-end/new-order/new-order.component';
import { ClientAddressComponent } from './front-end/client-address/client-address.component';
import { TypeProductComponent } from './front-end/type-product/type-product.component';
import { OperatorComponent } from './front-end/operator/operator.component';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import {AgmCoreModule, GoogleMapsAPIWrapper} from '@agm/core';
import { MyOrderComponent } from './front-end/my-order/my-order.component';
import { ModalComponent } from './front-end/modal/modal.component';
import { LoginComponent } from './front-end/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { TypeSaleComponent } from './front-end/type-sale/type-sale.component';
import { PlanComponent } from './front-end/plan/plan.component';
import { LinesComponent } from './front-end/lines/lines.component';
import { ServicesComponent } from './front-end/servicesSale/services.component';
import { SalesContractComponent } from './front-end/sales-contract/sales-contract.component';
import {NgxSelectModule} from 'ngx-select-ex';
import { DetailsSaleComponent } from './front-end/details-sale/details-sale.component';
import {NgxMaskModule} from 'ngx-mask';
import { ProposalComponent } from './front-end/proposal/proposal.component';
import { NewOpportunityComponent } from './front-end/new-opportunity/new-opportunity.component';
import { FinancialStatementComponent } from './front-end/financial-statement/financial-statement.component';
// import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {DatePipe, registerLocaleData, ɵregisterLocaleData} from '@angular/common';
import {AngularDateTimePickerModule} from 'angular2-datetimepicker';
import { ActivityComponent } from './front-end/activity/activity.component';
import { NewActivityComponent } from './front-end/new-activity/new-activity.component';
import { FooterComponent } from './front-end/footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    LoggedComponent,
    MenuComponent,
    HeadProfileComponent,
    DashboardComponent,
    AccountComponent,
    MyDatasComponent,
    BankAccountComponent,
    MyDocumentsComponent,
    NewOrderComponent,
    ClientAddressComponent,
    TypeProductComponent,
    OperatorComponent,
    MyOrderComponent,
    ModalComponent,
    LoginComponent,
    TypeSaleComponent,
    PlanComponent,
    LinesComponent,
    ServicesComponent,
    SalesContractComponent,
    DetailsSaleComponent,
    ProposalComponent,
    NewOpportunityComponent,
    FinancialStatementComponent,
    ActivityComponent,
    NewActivityComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    // AngularFontAwesomeModule,
    GoogleChartsModule,
    NgbModule,
    DragulaModule.forRoot(),
    HttpClientModule,
    FormsModule,
    // AngularFireModule.initializeApp(environment.firebase),
    // AngularFireDatabaseModule,
    GooglePlaceModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyA3KVNChScwiqQ2CG2gMYLl1JtzMcrsHbw'}), // <---
    FormsModule, // <---
    NgbModule.forRoot(), // <-
    BrowserAnimationsModule,
    NgxSelectModule,
    NgxMaskModule.forRoot(),
    // BsDatepickerModule.forRoot(),
    // DatepickerModule.forRoot(),
    AngularDateTimePickerModule,
    // GoogleChartsModule.forRoot(),
    NgxLoadingModule.forRoot({})
  ],
  providers: [GoogleMapsAPIWrapper, DatePipe],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalComponent,
    NewOpportunityComponent
  ]
})
export class AppModule { }
