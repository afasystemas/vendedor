import { Component, OnInit } from '@angular/core';
import {Plan} from '../../entity/Plan';
import {PlanService} from '../plan/plan.service';
import {Operator} from '../../entity/Operator';
import {SalesContractService} from './sales-contract.service';
import {City} from '../../entity/City';
import {State} from '../../entity/State';
import {CEP} from '../../entity/CEP';
import {NewOrderService} from '../new-order/new-order.service';
import {Demand} from '../../entity/Demand';

@Component({
  selector: 'app-sales-contract',
  templateUrl: './sales-contract.component.html',
  styleUrls: ['./sales-contract.component.css']
})
export class SalesContractComponent implements OnInit {

  operators: Array<Operator>;
  listStates: Array<State>;
  listCities: Array<City>;
  cep: CEP;
  cnpj: number;
  city: Array<String> = [];
  state: String;
  operatosView =  [];
  citiesView =  [];
  statesView =  [];
  operator: Operator;
  demandView: Demand;

  constructor(private salesContractService: SalesContractService, private newOrderService: NewOrderService) {
    this.operators = new Array<Operator>();
    this.operator = new Operator();
    this.demandView = this.newOrderService.newDemand;
console.log(this.newOrderService.newDemand);

    const self = this;
    this.salesContractService.operators.subscribe((data) => {
      data.forEach(function (value, key) {
        self.operatosView.push(value.name);
      });
    });
    this.salesContractService.states.subscribe((data) => {
      this.listStates = data;
      data.forEach(function (value, key) {
        self.statesView.push(value.name);
      });
      // console.log(this.listStates);
      // console.log(this.state);
      if (this.state !== undefined) {
        this.getCities(this.state);
      }
      // if (this.newOrderService.newDemand.address.cep.city.State.uf !== undefined) {
      //   this.state = this.listStates.find(obj => obj.uf === this.newOrderService.company.address.cep.city.State.uf).name;
      // }
    });

    this.salesContractService.cities.subscribe((data) => {
      this.citiesView = [];
      this.listCities = data;
      data.forEach(function (value, key) {
        self.citiesView.push(value.name);
      });
    });
    // this.cep = newOrderService.cep;
    // this.cnpj = this.newOrderService.cnpj;
  }

  ngOnInit() {
    this.salesContractService.getListServices();
    this.salesContractService.getListState();
    // this.cep = this.newOrderService.cep;
    this.cnpj = this.newOrderService.cnpj;
  }

  getCities(event) {
     this.salesContractService.getListCity((this.listStates.find( stat => stat.name === event)).id);
  }

}
