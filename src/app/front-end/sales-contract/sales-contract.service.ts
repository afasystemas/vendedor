import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SalesContractService {


  urlServiceOperator: String = environment.apiUrl + 'operator';
  urlServiceState: String = environment.apiUrl + 'state';
  urlServiceCity: String = environment.apiUrl + 'city';
  operators: EventEmitter<any> = new EventEmitter<any>();
  states: EventEmitter<any> = new EventEmitter<any>();
  cities: EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient,
              private routes: Router) { }

  public getListServices() {
    this.http.get(this.urlServiceOperator + '/list' ).subscribe(data => {
      console.log(data);
      this.operators.emit(data);
    });
  }
  public getListState() {
    this.http.get(this.urlServiceState + '/list' ).subscribe(data => {
      this.states.emit(data);
    });
  }
  public getListCity(idState: number) {
    this.http.get(this.urlServiceCity + '/list/' + idState ).subscribe(data => {
      this.cities.emit(data);
    });
  }
}
