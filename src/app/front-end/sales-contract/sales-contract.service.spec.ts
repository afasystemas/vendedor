import { TestBed, inject } from '@angular/core/testing';

import { SalesContractService } from './sales-contract.service';

describe('SalesContractService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesContractService]
    });
  });

  it('should be created', inject([SalesContractService], (service: SalesContractService) => {
    expect(service).toBeTruthy();
  }));
});
