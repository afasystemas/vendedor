import { Component, OnInit } from '@angular/core';
import {NewOrderService} from '../new-order/new-order.service';
import {TypeSalesService} from './type-sales.service';
import {TypeSale} from '../../entity/TypeSale';
import {Router} from '@angular/router';
import $ from 'jquery';
import {DemandTypeSale} from '../../entity/DemandTypeSale';

@Component({
  selector: 'app-type-sale',
  templateUrl: './type-sale.component.html',
  styleUrls: ['./type-sale.component.css']
})
export class TypeSaleComponent implements OnInit {

  listTypeSales: Array<TypeSale>;
  listChooseTypeSales: Array<TypeSale>;
  listSettingCheck: Array<number>;
  showbtnAvancar: boolean;
  constructor( private newOrderService: NewOrderService,
               private typeSalesService: TypeSalesService,
               private router: Router) {
    this.showbtnAvancar = false;
    this.typeSalesService.typeSales.subscribe((data) => {
      this.listTypeSales = data;
    });
    this.typeSalesService.getListTypeSales();
    if (this.newOrderService.listTypeSale.length > 0 ) {
      this.listTypeSales = this.newOrderService.listTypeSale;
    }
    this.listSettingCheck = [];
  }

  ngOnInit() {
    this.listTypeSales = [];
    this.listChooseTypeSales = this.newOrderService.listTypeSale;

    if (this.newOrderService.newDemand.id !== 0) {
      this.showbtnAvancar = true;
    }

    if (this.newOrderService.listDemandTypeSales.length > 0 ) {
      this.newOrderService.listDemandTypeSales.forEach((value, ind) => {
          this.listSettingCheck.push(value.typesaleid);
      });

    }
  }

  addTypeSale(indexItemFromServiceArray: number, elem) {
    const nameTypeSale = this.listTypeSales[indexItemFromServiceArray];
    const deman = this.newOrderService.newDemand;
    const demandTS = new DemandTypeSale();

    if (elem.currentTarget.checked) {
      const ts = this.listTypeSales[indexItemFromServiceArray];
      this.listChooseTypeSales.push(ts);
    } else if (!elem.currentTarget.checked) {
      const ts = this.listTypeSales[indexItemFromServiceArray];
      const i = this.listChooseTypeSales.indexOf(ts);
      this.listChooseTypeSales.splice(i, 1);
    }
    switch (indexItemFromServiceArray) {
      case 0: // caso seja "novo"
        const remove = [2, 5]; // remover "Renovação" e "Incremento",
        this.removeCombination(remove);
        this.uncheck(remove);
        break;
      case 1 : // caso seja "Renovação"
      case 4 : // caso seja "Incremento"
        const removeB = [1, 3]; // remover "Novo" e "TT"
        this.removeCombination(removeB);
        this.uncheck(removeB);
        break;
      case 2 : // caso seja "TT"
        const removeC = [2]; // remover "Renovação"
        this.removeCombination(removeC);
        this.uncheck(removeC);
        break;
      // case 4 : // caso "Incremento"
      //   const removeD = [1, 3]; // remover "Novo" e "TT"
      //   this.removeCombination(remove);
      //   this.uncheck(removeD)
      //   break;
    }

    this.newOrderService.listDemandTypeSales = [];

    const dem = this.newOrderService.newDemand;
    this.listChooseTypeSales.forEach(obj => {
      const dts = new DemandTypeSale();
      dts.demandid = dem.id;
      dts.typeSale = obj;
      this.newOrderService.listDemandTypeSales.push(dts);
    });

    // console.log($('input:checked'));
    // console.log($('input:not checked'));
    // console.log(elem.currentTarget.checked)
    // console.log(indexItemFromServiceArray);
    //
    // console.log(this.newOrderService.listDemandTypeSales);


  }
  nextPass() {
    // this.newOrderService.listChooseTypeSale.emit(this.listChooseTypeSales);
    // const self = this;
    // const listdts = Array<DemandTypeSale>();
    // this.newOrderService.newDemand.typesSale.forEach(function (obj) {
    //   const ts = new DemandTypeSale()
    //   ts.typeSale = obj;
    //   listdts.push(ts);
    // });
    // this.newOrderService.typeSales.emit(listdts);
    this.newOrderService.updateTypeSaleOpportunity();
  }
  uncheck (ids) {
    for (let i = 0 ; i < ids.length; i++) {
      $('#customCheck' + ids[i]).prop( 'checked', false );
    }
  }
  removeCombination(listRemove) {
    listRemove.forEach(num => {
      let i = -1;
      this.listChooseTypeSales.forEach((obj, ind) => {
        if (obj.id === num) {
          i = ind;
        }
      })

      if (i > -1 ) {
        this.listChooseTypeSales.splice(i, 1);
      }
    });
  }
}
