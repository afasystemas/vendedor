import { TestBed, inject } from '@angular/core/testing';

import { TypeSalesService } from './type-sales.service';

describe('TypeSalesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TypeSalesService]
    });
  });

  it('should be created', inject([TypeSalesService], (service: TypeSalesService) => {
    expect(service).toBeTruthy();
  }));
});
