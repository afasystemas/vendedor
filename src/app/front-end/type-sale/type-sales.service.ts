import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TypeSalesService {

  urlTypeSales: String = environment.apiUrl + 'typesales';
  typeSales: EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient,
              private routes: Router) { }

  public getListTypeSales() {
    this.http.get(this.urlTypeSales + '/list' ).subscribe(data => {
      this.typeSales.emit(data);
    });
  }
}
