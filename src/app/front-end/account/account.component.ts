import { Component, OnInit } from '@angular/core';
import $ from 'jquery';


@Component({
  selector: 'app-my-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  constructor() {}

  ngOnInit() {
  }

  activeItemLink(idElement: string) {
    $('#' + idElement).addClass('active');
    $('.nav-link:not(#' + idElement + ')').removeClass('active');
  }
}
