import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDatasComponent } from './my-datas.component';

describe('MyDatasComponent', () => {
  let component: MyDatasComponent;
  let fixture: ComponentFixture<MyDatasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDatasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
