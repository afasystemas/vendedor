import { Component, OnInit } from '@angular/core';
import {LoginService} from '../login/login.service';
import {User} from '../../entity/User';
import {environment} from '../../../environments/environment.prod';

@Component({
  selector: 'app-my-datas',
  templateUrl: './my-datas.component.html',
  styleUrls: ['./my-datas.component.css']
})
export class MyDatasComponent implements OnInit {

  user: User;
  photoProfile: String  = environment.apiUrlImage + '1.jpg';
  urlInstagran: String  = environment.apiUrlIcon + 'instagram-logo.png';
  urlFacebook: String  = environment.apiUrlIcon + 'facebook-logo.png';
  constructor(private loginService: LoginService) {
    this.loginService.user.subscribe(data => {
      this.user = data;
      this.photoProfile = this.user.urlphotoprofile;
      console.log(this.photoProfile);
    });
    if (this.loginService.userLogged) {
      this.user = loginService.userLogged;
    }

  }

  ngOnInit() {
  }

}
