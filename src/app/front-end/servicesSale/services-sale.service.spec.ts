import { TestBed, inject } from '@angular/core/testing';

import { ServicesSaleService } from './services-sale.service';

describe('ServicesSaleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServicesSaleService]
    });
  });

  it('should be created', inject([ServicesSaleService], (service: ServicesSaleService) => {
    expect(service).toBeTruthy();
  }));
});
