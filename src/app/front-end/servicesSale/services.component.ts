import { Component, OnInit } from '@angular/core';
import {ServicesSaleService} from './services-sale.service';
import {Service} from '../../entity/Service';
import $ from 'jquery';
import {LinesServiceService} from '../lines/lines-service.service';
import {Router} from '@angular/router';
import {NewOrderService} from '../new-order/new-order.service';
import index from '@angular/cli/lib/cli';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {


  services: Array<Service>;
  total: number;
  constructor(private serviceSale: ServicesSaleService, private router: Router, private newOrderService: NewOrderService) {
    this.services = [];
    this.newOrderService.services.subscribe((data) => {
      this.services = data;
    });
    this.services = this.newOrderService.listServices;

    this.total = 0;
  }

  ngOnInit() {
  }

  calculatorTotalService(indexItemFromServiceArray: number, elem) {
    console.log(elem.currentTarget.checked);
    console.log(Number(this.services[indexItemFromServiceArray].value));
    console.log(this.newOrderService.listServices);
    if (elem.currentTarget.checked) { console.log('checado');
      this.newOrderService.listServices.push(this.services[indexItemFromServiceArray]);
      let totalValueService = 0;
      this.newOrderService.listServices.forEach(function (obj, ind) {
        totalValueService = totalValueService + obj.value;
      });
       const sumTotal =
         Number.parseFloat(
           (Number.parseFloat(this.newOrderService.totalValueDemand.toString()) +
           Number.parseFloat(totalValueService.toString())).toFixed(2));
      // this.newOrderService.sumTotal(sumTotal);
      console.log(this.newOrderService.totalValueDemand);
      this.total = Number((Number(this.total) + Number(this.services[indexItemFromServiceArray].value)).toFixed(2));
    } else {console.log('desmarcado');
      const indx = this.newOrderService.listServices.indexOf(this.services[indexItemFromServiceArray]);
      console.log(indx);
      this.newOrderService.listServices.splice(indx, 1);
      console.log(this.newOrderService.listServices);
      let totalValueService = 0;
      this.newOrderService.listServices.forEach(function (obj, ind) {
        totalValueService = totalValueService + obj.value;
      });
      this.total = totalValueService;
      // const sumTotal =
      //   Number.parseFloat(
      //     (Number.parseFloat(this.newOrderService.totalValueDemand.toString()) +
      //       Number.parseFloat(totalValueService.toString())).toFixed(2));
      // this.newOrderService.totalValueEvent.emit(sumTotal);
      const totalServic =
        Number.parseFloat((Number.parseFloat(this.newOrderService.totalValueDemand.toString()) -
          Number.parseFloat(this.services[indexItemFromServiceArray].value.toString())).toFixed(2));
      console.log(this.total);
      // this.newOrderService.reduceTotal(totalServic);
    }
  }

  nextPass() {
    // this.newOrderService.listChooseTypeSale.emit(this.listChooseTypeSales);
    this.router.navigate(['/logged/nova-venda/papelada']);

  }
}
