import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {NewOrderService} from '../new-order/new-order.service';
import {Service} from '../../entity/Service';

@Injectable({
  providedIn: 'root'
})
export class ServicesSaleService {

  urlServiceSale: String = environment.apiUrl + 'service';
  idOperator = '1';
  idPlan = '2';
  services:  EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient,
              private routes: Router
              ) {
  }

  public getListServiceFilter(keyword: string) {
    this.http.get(this.urlServiceSale + '/list/' + this.idOperator + '/' + keyword ).subscribe(data => {
      this.services.emit(data);
    });
  }
}
