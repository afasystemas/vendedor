import { TestBed, inject } from '@angular/core/testing';

import { NewOpportunityService } from './new-opportunity.service';

describe('NewOpportunityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewOpportunityService]
    });
  });

  it('should be created', inject([NewOpportunityService], (service: NewOpportunityService) => {
    expect(service).toBeTruthy();
  }));
});
