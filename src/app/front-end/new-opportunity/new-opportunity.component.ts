import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {NewOrderService} from '../new-order/new-order.service';
import {ClientAddressService} from '../client-address/client-address.service';
import {NewOpportunityService} from './new-opportunity.service';

@Component({
  selector: 'app-new-opportunity',
  templateUrl: './new-opportunity.component.html',
  styleUrls: ['./new-opportunity.component.css']
})
export class NewOpportunityComponent implements OnInit {

  code: number;
  alertCode: boolean;
  mgsAlert = 'Informe o Código do pedido!';

  constructor(public activeModal: NgbActiveModal,
              private service: NewOpportunityService,
              private newOrderService: NewOrderService) {
    this.alertCode = false;
  }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.close(false);
  }

  confirmModal() {
    this.activeModal.close(true);
  }

  searchDemandByCode() {
    if (this.code !== undefined) {
      // this.service.getAddresCompanyByCnpj(this.code.toString());
      /*18058323000100*/
      this.newOrderService.getDemandByCode(this.code)
      this.confirmModal();
    } else {
      this.alertCode = true;
      setTimeout(() => {
          this.alertCode = false;
        },
        5000);
    }


  }

}
