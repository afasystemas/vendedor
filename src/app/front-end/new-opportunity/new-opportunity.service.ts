import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NewOpportunityService {

  url: String = environment.apiUrl + 'demand/opportunity/';

  constructor(private http: HttpClient, private routes: Router) { }

  public getAddresCompanyByCnpj(cnpj: string): any {
    // const body = JSON.stringify(user);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json');
    // const options = {
    //   headers: headers
    // };
    this.http.get(this.url + cnpj ).subscribe(data => {
      console.log(data);
    });
  }
}
