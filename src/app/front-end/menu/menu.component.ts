import {AfterViewInit, Component, ElementRef, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import $ from 'jquery';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {LoggedComponent} from '../logged/logged.component';
import {LoginService} from '../login/login.service';


declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/logged/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/logged/minhas-vendas', title: 'Funil de Vendas',  icon: 'ni-pin-3 text-orange', class: '' },
  { path: '/logged/nova-venda/cliente', title: 'Proposta',  icon: 'ni ni-money-coins', class: 'text-blue' },
  { path: '/logged/extrato-financeiro', title: 'Extrato Financeiro',  icon: 'ni-single-02 text-yellow', class: '' },
  // { path: '/tables', title: 'Tables',  icon: 'ni-bullet-list-67 text-red', class: '' },
  // { path: '/login', title: 'Login',  icon: 'ni-key-25 text-info', class: '' },
  // { path: '/register', title: 'Register',  icon: 'ni-circle-08 text-pink', class: '' }
];
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  animations: [
    trigger('slideInOut', [
      state('out', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('in', style({
        transform: 'translate3d(15%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('containerMenuStatus', [
      state('out', style({
        transform: 'translate3d(10%, 0, 0)'
      })),
      state('in', style({
        transform: 'translate3d(0%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
     trigger('textMenuInOut', [
      state('out', style({
        visibility: 'hidden',
        width: '0px'
      })),
      state('in', style({
        visibility: 'visible'
      })),
      // transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('title-session', [
      state('out', style({
        'font-size': '0.8em'
      })),
      state('in', style({
        'font-size': '1em'
      })),
      // transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),


  ]
})
export class MenuComponent implements OnInit {


  public menuItems: any[];
  public isCollapsed = true;


  // @Inject(LoggedComponent) containerBody: LoggedComponent;
  menuState: String = 'out';
  menuTextState: String = 'in';
  constructor(private containerBody: LoggedComponent, private loginService: LoginService,
              private router: Router) {
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }

  toggleMenu() {
    // 1-line if statement that toggles the value:
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
    this.menuTextState = this.menuTextState === 'out' ? 'in' : 'out';
    this.containerBody.statusResponsive();
  }

  logOut() {
    this.loginService.token.emit(null);
  }

}
