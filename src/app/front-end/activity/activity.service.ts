import {EventEmitter, Injectable} from '@angular/core';
import {Demand} from '../../entity/Demand';
import {Activity} from '../../entity/Activity';
import {StatusSale} from '../../entity/StatusSale';
import {OrderStatus} from '../../entity/OrderStatus';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  sale: Demand;
  activities: EventEmitter<any> = new EventEmitter<any>();
  urlHistoric: String = environment.apiUrl + 'salehistoric/';


  constructor(private http: HttpClient,
              private routes: Router) { }

  public getHistoricFromDemand(code: string) {
    const salesmanId = localStorage.getItem('userid');
    this.http.get(this.urlHistoric + 'listByDemand/' + code).subscribe(data => {
      console.log(data);
      this.activities.emit(data);

    });
  }
}
