import { Component, OnInit } from '@angular/core';
import {ActivityService} from './activity.service';
import {ActivatedRoute} from '@angular/router';
import {Demand} from '../../entity/Demand';
import {Activity} from '../../entity/Activity';
import {Company} from '../../entity/Company';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {

  code: string;
  demand: Demand;
  company: Company;
  activities: Array<Activity>;
  constructor(private activityService: ActivityService, private route: ActivatedRoute) {
    this.demand = new Demand();
    this.company = new Company();
    this.activities = new Array<Activity>();
    this.activityService.activities.subscribe(data => {
      console.log(data);
      this.company = data[0].SaleHistoric.Demand.Company;
      this.demand = data[0].SaleHistoric.Demand;
      this.activities = data;
    });
    this.route.params.subscribe(params => {
      this.code = params['code']; // (+) converts string 'id' to a number
      console.log(this.code);
      this.activityService.getHistoricFromDemand(this.code);
    });
  }


  ngOnInit() {
  }

}
