import {EventEmitter, Injectable} from '@angular/core';
import {Demand} from '../../entity/Demand';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';

import {TypeSale} from '../../entity/TypeSale';
import {Address} from '../../entity/Address';
import {Company} from '../../entity/Company';
import {Street} from '../../entity/Street';
import {CEP} from '../../entity/CEP';
import {Neighborhood} from '../../entity/Neighborhood';
import {Plan} from '../../entity/Plan';
import {Package} from '../../entity/Package';
import {PlanPackage} from '../../entity/PlanPackage';
import {DemandPhone} from '../../entity/DemandPhone';
import {Phone} from '../../entity/Phone';
import {DemandProduct} from '../../entity/DemandProduct';
import {Service} from '../../entity/Service';
import {TitlePlan} from '../../entity/TitlePlan';
import {ServicesSaleService} from '../servicesSale/services-sale.service';
import {environment} from '../../../environments/environment.prod';
import {PlanService} from '../../entity/PlanService';
import {CompanyPhone} from '../../entity/CompanyPhone';
import {User} from '../../entity/User';
import set = Reflect.set;
import {Person} from '../../entity/Person';
import {PhoneContacts} from '../../entity/PhoneContacts';
import {DemandTypeSale} from '../../entity/DemandTypeSale';
import {ServiceDemandPhone} from '../../entity/ServiceDemandPhone';

@Injectable({
  providedIn: 'root'
})
export class NewOrderService {

  newDemand: Demand;
  // addressClient: Address;
  addressComplete: string;
  addressCompleteShow: string;
  clientDatasReceita: string;
  cnpj: number;
  // company: Company;
  // neighborhood: Neighborhood;
  // cep: CEP;
  // street: Street;
  listTypeSale: Array<TypeSale>;
  plan: Plan;
  // phoneLines: Array<string>;
  ddd: any;
  totalValueDemand: number;
  listPackagesFromPlan: Array<PlanPackage>;
  listTitlePlans: Array<TitlePlan>;
  listContacts: Array<Person>;
  membercompany: Person;
  contactCompany: Person;
  phonesContactsCompanyList: string;
  listPhoneContactsCompany: Array<Phone>;
  listCompanyPhones: Array<CompanyPhone>;
  listServiceDemandPhone: Array<ServiceDemandPhone>; /*lista de telefones e package do pedido que serão cadastradas no pedido*/
  listDemandPhone: Array<DemandPhone>; /*lista de telefones e package do pedido que serão cadastradas no pedido*/
  listDemandProducts: Array<DemandProduct>; /*lista de produtos do pedido que serão cadastradas no pedido*/
  listDemandServices: Array<PlanService>; /*lista de servicos do pedido que serão cadastradas no pedido*/
  listDemandTypeSales: Array<DemandTypeSale>; /*lista de tipo de venda do pedido*/
  listServices: Array<Service>; /*lista de servicos do pedido que serão cadastradas no pedido*/
  listPlanServices: Array<PlanService>; /*lista de servicos do pedido que serão cadastradas no pedido*/
  listPhone: Array<Phone>; /*lista de telefones que serão cadastradas no pedido*/
  companyPhones: Array<Phone>; /*lista de telefones da Company/Empresa*/

  demandEvent: EventEmitter<any> = new EventEmitter<any>();
  cnpjEvent: EventEmitter<any> = new EventEmitter<any>();
  phoneLineEvent: EventEmitter<any> = new EventEmitter<any>();
  typeSales: EventEmitter<any> = new EventEmitter<any>();
  viewTypeSales: EventEmitter<any> = new EventEmitter<any>();
  planEvent: EventEmitter<any> = new EventEmitter<any>();
  lines: EventEmitter<any> = new EventEmitter<any>();
  demandPhoneEvent: EventEmitter<any> = new EventEmitter<any>();
  products: EventEmitter<any> = new EventEmitter<any>();
  packages: EventEmitter<any> = new EventEmitter<any>();
  services: EventEmitter<any> = new EventEmitter<any>();
  packagesEvent: EventEmitter<any> = new EventEmitter<any>();
  totalValueEvent: EventEmitter<number> = new EventEmitter<number>();
  listTitlesPlanEvent: EventEmitter<any> = new EventEmitter<any>();
  razaosocialEvent: EventEmitter<any> = new EventEmitter<any>();
  destroyerEvent: EventEmitter<any> = new EventEmitter<any>();
  loadingSaveEvent: EventEmitter<any> = new EventEmitter<any>();

  // listChooseTypeSale: EventEmitter<any> = new EventEmitter();

  urlServiceSale: String = environment.apiUrl + 'service';
  urldemand: String = environment.apiUrl + 'demand';
  idOperator = '1';

  mgsPackages: string;

  totalsum: number;
  totalPack: number;
  totalProd: number;
  totalServ: number;


  constructor( private http: HttpClient,
               private routes: Router,
               servicesale: ServicesSaleService) {
    this.newDemand = new Demand();
    // this.company = new Company();
    // this.addressClient = new Address();

    this.plan = new Plan();
    this.addressComplete = '';
    this.clientDatasReceita = '';
    // this.cnpj ;
    // this.neighborhood = new Neighborhood();
    // this.street = new Street();
    this.listTypeSale = [];
    this.listPackagesFromPlan = [];
    // this.phoneLines = [];
    this.listPhone = [];
    this.listContacts = [];
    this.phonesContactsCompanyList = '';
    this.membercompany = new Person();
    this.contactCompany = new Person();
    this.listPhoneContactsCompany = [];
    this.companyPhones = [];
    this.listServiceDemandPhone = [];
    this.listDemandPhone = [];
    this.listDemandProducts = [];
    this.listDemandServices = [];
    this.listDemandTypeSales = Array<DemandTypeSale>();
    this.listServices = [];
    this.listPlanServices = [];
    this.totalValueDemand = 0;
    const totalsum = 0;
    const totalPack = 0;
    const totalProd = 0;
    const totalServ = 0;

    // this.listChooseTypeSale.subscribe((data) => {
    //   this.listTypeSale = data;
    // });
    const self = this;
    this.demandEvent.subscribe(data => {
      this.newDemand = data;
      console.log(this.newDemand);
    });
    this.cnpjEvent.subscribe((data) => {
      this.cnpj = data;
    });
    this.totalValueEvent.subscribe((data) => {
      this.totalValueDemand = data;
    });
    this.packagesEvent.subscribe((data) => {
      self.listPackagesFromPlan = data;
    });
    this.products.subscribe(data => {
      this.listDemandProducts = data;
      this.sumTotal();
    });
    this.demandPhoneEvent.subscribe(data => {
      this.listDemandPhone = data;
      this.sumTotal();
    });
    this.listTitlesPlanEvent.subscribe(data => {
      this.listTitlePlans = data;
    });
    this.planEvent.subscribe(data => {
      console.log(data);
      this.plan = data;
      this.getListServiceByPlan(this.plan.id);
    });
    this.typeSales.subscribe(data => {
      this.listDemandTypeSales = data;
    });
  }
  clearSale() {

  }
  private sumTotal() {
    let sumValueDemandPhon = 0;
    // console.log(this.listDemandPhone);
    // console.log(this.listDemandServices);
    // total para DemandPhone
    this.listDemandPhone.forEach(function (obj, index) {
      obj.listserviceDemandPhone.forEach(function (servs , ind) {
        sumValueDemandPhon += servs.titleplan.valueTotal;
        // const totalDemandPhone =
        //   servs.titleplan.valueTotal + obj.planService.value;
        // obj.totalValue = totalDemandPhone;
      });


    });
    let sumValueProd = 0;
    this.listDemandProducts.forEach(function (obj, ind) {
      sumValueProd +=  obj.total;
    });
    let sumValueService = 0;
    this.listDemandServices.forEach(function (obj, ind) {
      sumValueService +=  obj.value;
    });



    this.totalsum = Number.parseFloat((sumValueDemandPhon + sumValueProd + sumValueService).toFixed(2).toString());
    this.totalPack = sumValueDemandPhon;
    this.totalProd = sumValueProd;
    this.totalServ = sumValueService;
    // console.log(this.totalPack);
    // console.log(this.totalProd);
    // console.log(this.totalServ);
    // console.log(this.totalsum);
    // console.log(this.totalPack + this.totalProd + this.totalServ);

    const sumTotal = Number.parseFloat((this.totalPack + this.totalProd + this.totalServ).toFixed(2).toString());
    // console.log(sumTotal);
    // console.log(this.totalValueDemand);
    this.totalValueEvent.emit(sumTotal);
    // console.log(this.listDemandPhone);
  }

  // reduceTotal( valueReduce: number) {
  //   const reduceTotal = Number.parseFloat((Number.parseFloat(this.totalValueDemand.toFixed(2).toString())
  //     - Number.parseFloat(valueReduce.toFixed(2).toString())).toFixed(2));
  //   this.totalValueEvent.emit(reduceTotal);
  // }

  public getListServiceByPlan(idPlan) {
    const self = this;
    this.http.get(this.urlServiceSale + '/listadicionais/' + this.idOperator + '/' + idPlan).subscribe(data => {
      console.log(data);
      // @ts-ignore
      this.listPlanServices = data;
      if (data) {
        // @ts-ignore
        data.forEach(function (obj, val) {
          self.listServices.push(obj.Service);
        });
        this.services.emit(this.listServices);
      } else {
        this.services.emit(data);
      }
    });
  }

  public getDemandByCode(code) {
    const self = this;
    this.http.get(this.urldemand + '/code/' + code).subscribe(data => {
      this.demandEvent.emit(data);
    });
  }

  async saveOpportunity() {
    this.loadingSaveEvent.emit(true);
    const self = this;
    const payload = new FormData();
    await this.removePhoneEqual();
    // payload.append('demand', this.newDemand);
    // payload.append('phones', JSON.parse(JSON.stringify(this.companyPhones)));
    console.log(this.listPhoneContactsCompany);
   // return this.http.post('/api/v1/invoices/', payload, HttpUploadOptions)
    const httpParams = new HttpParams();
    //   .set('demand': JSON.stringify(this.newDemand))
    // .set('phones': JSON.stringify(this.companyPhones));
    httpParams.append('demand', JSON.stringify(this.newDemand));
    // httpParams.append('phones', JSON.stringify(this.newDemand));
    const obj = {
      demand: this.newDemand,
      user: Number.parseInt(localStorage.getItem('userid')),
      listPhoneContactsCompany: this.listPhoneContactsCompany,
      listPerson: this.listContacts
    };
    const body = JSON.stringify(obj);

    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = {
      headers: headers
    };
    this.http.post(this.urldemand + '/opportunity', body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
        .set('apikey', environment.KeySecurity),
    }).subscribe(data => {
      if (data) {
        console.log(data);
        // @ts-ignore
        this.demandEvent.emit(data.demand);
        // @ts-ignore
        this.membercompany = data.phonecontactmember;
        // @ts-ignore
        this.contactCompany = data.contactperson.person;
        // @ts-ignore
        this.contactCompany.phone = data.contactperson.phone;
        // @ts-ignore
        this.phonesContactsCompanyList = data.phonecontactcompany;

        this.loadingSaveEvent.emit(false);
        this.routes.navigate(['/logged/nova-venda/tipo-venda']);
      } else {
        this.loadingSaveEvent.emit(false);
      }
    });
  }


  removePhoneEqual(): void {
    const filterPhonesContactsCompany = [];
    const sizrArrray = this.listPhoneContactsCompany.length;
    for (let i = 0 ; i < sizrArrray; i++) {

      const index = filterPhonesContactsCompany.indexOf(this.listPhoneContactsCompany[i]);
      console.log(index);
      if (index === -1) {
        filterPhonesContactsCompany.push(this.listPhoneContactsCompany[i]);
      }
    }
    this.listPhoneContactsCompany = filterPhonesContactsCompany;
      console.log('valor da lista depois');
    console.log(this.listPhoneContactsCompany );
  }

  async updateTypeSaleOpportunity() {
    // this.newDemand.typesSale.forEach(function (obj) {
    //   const ts = new DemandTypeSale()
    //   ts.typeSale = obj;
    //   this.listDemandTypeSales.push(ts);
    // });
    this.newDemand.plan = this.plan;

    const obj = {
      demand: this.newDemand,
      listDemandTypeSale: this.listDemandTypeSales,
      user: Number.parseInt(localStorage.getItem('userid')),
    };
    const body = JSON.stringify(obj);
    console.log(obj);
    console.log(this.plan);

    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = {
      headers: headers
    };

    this.http.post(this.urldemand + '/typesale/update', body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
        .set('apikey', environment.KeySecurity),
    }).subscribe(data => {
      if (data) {
        // @ts-ignore
        console.log(data);
        // @ts-ignore
        console.log(data.listDemandTypeSale);
        // @ts-ignore
        this.listDemandTypeSales = data.listDemandTypeSale;
        // @ts-ignore
        console.log(data.demand);
        // @ts-ignore
        this.newDemand = data.demand;


        this.loadingSaveEvent.emit(false);
        // this.routes.navigate(['/logged/nova-venda/linhas']);
      } else {
        this.loadingSaveEvent.emit(false);
      }
    });
  }
}
