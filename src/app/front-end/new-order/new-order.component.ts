import {Component, OnDestroy, OnInit} from '@angular/core';
import $ from 'jquery';
import {Router} from '@angular/router';
import {NewOrderService} from './new-order.service';
import {ServicesSaleService} from '../servicesSale/services-sale.service';
import {NewOpportunityComponent} from '../new-opportunity/new-opportunity.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ClientAddressComponent} from '../client-address/client-address.component';


@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css']
})
export class NewOrderComponent implements OnInit, OnDestroy {

  showGenerateProporsal: Boolean = false;
  showSearchDemand: Boolean = true;
  checkCnpjAddress: Boolean = false;
  checkTypeSale: Boolean = false;
  totalValueSale: number;
  showService = false;

  razaosocial = '';
  cnpjProporsal = '';
  cnpjMask = '00.000.000/0000-00';

  constructor(private router: Router,
              private modalService: NgbModal,
              private newOrderService: NewOrderService,
              private serviceSale: ServicesSaleService) {

    this.showGenerateProporsal = true;

    this.router.events.subscribe((data) => {
      // console.log(data);
      // if (data.url) {
      //   const ur = "a[href='" + data.url + "']";
      //   console.log(ur);
      //   console.log($(ur));
      // }
      // console.log($('a[href$="' + data.url + '"]').id));
    });
    this.newOrderService.cnpjEvent.subscribe((data) => {
      if (data !== undefined && data !== '' && data !== null) {
        this.checkCnpjAddress = true;
      } else {
        this.checkCnpjAddress = false;
      }
    });
    this.newOrderService.services.subscribe(data => {
      if (this.newOrderService.listPlanServices.length > 0 ) {
        this.showService = true;
      } else {
        this.showService = false;
      }
    });

    this.totalValueSale = this.newOrderService.totalValueDemand;
    this.newOrderService.totalValueEvent.subscribe((data) => {
      this.totalValueSale = Number.parseFloat(data);
    });
    this.newOrderService.razaosocialEvent.subscribe(data => {
      this.razaosocial = data;
    });
    this.newOrderService.cnpjEvent.subscribe(data => {
      this.cnpjProporsal = data;
    });
  }

  ngOnInit() {}

  generateProporsal() {
    // this.checkCnpjAddress = true;
    console.log(this.newOrderService.newDemand);
    const win = window.open('/proposta', '_blank');
    // this.router.navigate(['/logged/nova-venda/tipo-venda']);
    win.focus();
  }

  // activeItemLink(idElement: string) {console.log(idElement);
    // $('#' + idElement).addClass('active');
    // $('.nav-link:not(#' + idElement + ')').removeClass('active');
  // }

  ngOnDestroy(): void {
    this.newOrderService.destroyerEvent.emit(true);
  }

  getDemand() {
    this.modalService.open(NewOpportunityComponent).result.then((result) => {

      if (!result) {

      } else {
        // fazer chamada para api da lista de oportunidades buscando a
        // nova lista depois de ter acrescentado uma nova oportunidade
      }
    }).catch((error) => {
      console.log(error);
    });
  }
}
