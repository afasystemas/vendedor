import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {NewOrderService} from '../new-order/new-order.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  mgsModal: string;
  constructor(public activeModal: NgbActiveModal, private newOrderService: NewOrderService) {
    this.mgsModal = this.newOrderService.mgsPackages;
  }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.close(false);
  }

  confirmModal() {
    this.activeModal.close(true);
  }

}
