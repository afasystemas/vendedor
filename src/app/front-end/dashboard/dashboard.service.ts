import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  urlDemand: String = environment.apiUrl + 'demand';
  resultDataDashboard: EventEmitter<any> = new EventEmitter<any>();
  resultDataWeekDashboard: EventEmitter<any> = new EventEmitter<any>();
  resultDataMonthDashboard: EventEmitter<any> = new EventEmitter<any>();
  resultDataYearlyDashboard: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private http: HttpClient,
    private routes: Router
  ) {}

  public getDataToDashBoard(): void {
    const salesmanId = localStorage.getItem('userid');
    // console.log('chamei a api');
    this.http.get(this.urlDemand + '/dashboard/' + salesmanId ).subscribe(data => {
      // console.log(data);
      // @ts-ignore
      this.resultDataDashboard.emit(data);
    });
  }
  public getDataWeekToDashBoard(): void {
    const salesmanId = localStorage.getItem('userid');
    // console.log('chamei a api');/dashboard/:id/salesweek
    this.http.get(this.urlDemand + '/dashboard/' + salesmanId + '/salesweek' ).subscribe(data => {
      // console.log(data);
      // @ts-ignore
      this.resultDataWeekDashboard.emit(data);
    });
  }
  public getDataMonthToDashBoard(): void {
    const salesmanId = localStorage.getItem('userid');
    // console.log('chamei a api');/dashboard/:id/salesweek
    this.http.get(this.urlDemand + '/dashboard/' + salesmanId + '/salesmonth' ).subscribe(data => {
      // console.log(data);
      // @ts-ignore
      this.resultDataMonthDashboard.emit(data);
    });
  }
  public getDataYearlyToDashBoard(): void {
    const salesmanId = localStorage.getItem('userid');
    // console.log('chamei a api');/dashboard/:id/salesweek
    this.http.get(this.urlDemand + '/dashboard/' + salesmanId + '/salesyearly' ).subscribe(data => {
      // console.log(data);
      // @ts-ignore
      this.resultDataYearlyDashboard.emit(data);
    });
  }
}
