import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {DashboardService} from './dashboard.service';
import Chart from 'chart.js';
import $ from 'jquery';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from '../../variables/charts';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  /*newdata do novo layout*/
  public datasets: any;
  public dataLabels: any;
  public dataNew: any;
  public dataDays: Array<String>;
  public days: Array<String>;
  public salesChart;
  // public days: Array<String>;
  public btnMonth: Boolean = true;
  public btnWeek: Boolean = false;
  public btnYears: Boolean = false;
  /*fim*/

  urlclient1 = environment.apiUrlIcon + '1.jpg';
  urlclient2 = environment.apiUrlIcon + '2.jpg';
  urlclient3 = environment.apiUrlIcon + '3.jpg';
  urlclient4 = environment.apiUrlIcon + '4.jpg';
  urlclient5 = environment.apiUrlIcon + '5.jpg';
  title = 'Histórico de Vendas 2019';
  type = 'LineChart';
  data = [
    ['Jan',  17.0],
    ['Feb',  16.9],
    ['Mar',  19.5],
    ['Abr',  21.5],
    ['Mai',  18.2],
    ['Jun',  21.5],
    ['Jul',  23.2],
    ['Ago',  19.5],
    ['Set',  22.3],
    ['Out',  23.3],
    ['Nov',  21.9],
    ['Dez',  22.6]
  ];
  columnNames = ['Meses'];
  options = {
    chartArea: {
      // left: '3%',
      // top: '3%',
      height: '70%',
      width: '70%'
    },
    hAxis: {
      title: 'Meses'
    },
    vAxis: {
      title: 'Valores'
    },
  };

  titleMonth = 'Histórico de Vendas 2019';
  typeMonth = 'LineChart';
  dataMont = [
    ['Jan',  17.0],
    ['Feb',  16.9],
    ['Mar',  19.5],
    ['Abr',  21.5],
    ['Mai',  18.2],
    ['Jun',  21.5],
    ['Jul',  23.2],
    ['Ago',  19.5],
    ['Set',  22.3],
    ['Out',  23.3],
    ['Nov',  21.9],
    ['Dez',  22.6]
  ];
  columnNamesMonth = ['Meses'];
  optionsMonth = {
    chartArea: {
      // left: '3%',
      // top: '3%',
      height: '70%',
      width: '70%'
    },
    hAxis: {
      title: 'Meses'
    },
    vAxis: {
      title: 'Valores'
    },
  };

  imgHandBagMoney = environment.apiUrlIcon + 'hand-bag-money.png';

  negociacao: string;
  mesa: string;
  validacao: string;
  credito: string;
  concluido: string;
  portames: string;
  ativado: string;
  reprovadocred: string;

  valuesWeek: Array<number>;
  firstAccessGrafic = true;

  constructor(private dashBoardService: DashboardService) {
    this.negociacao = '';
    this.mesa = '';
    this.validacao = '';
    this.credito = '';
    this.concluido = '';
    this.portames = '';
    this.ativado = '';
    this.reprovadocred = '';
    const date =  new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate();
    this.datasets = [0];
    this.valuesWeek = [0];

    this.days = [];
    this.dataDays = [];
    // for (let i = 1; i <= date; i++) {
    //  this.days.push(i.toString());
    //  this.dataDays.push((Math.floor(Math.random() * (900 - 0 + 1)) + 0).toString());
    // }

    this.dashBoardService.resultDataDashboard.subscribe(data => {
      this.negociacao = data.negociacao;
      this.mesa = data.mesa;
      this.validacao = data.validacao;
      this.credito = data.credito;
      this.concluido = data.concluido;
      this.portames = data.portames;
      this.ativado = data.ativado;
      this.reprovadocred = data.reprovadocred;
      console.log(data.reprovadocred);
    });

    this.dashBoardService.resultDataWeekDashboard.subscribe(dataweek => {
      // console.log(dataweek);
      const minhaArray = [];
      // @ts-ignore
      dataweek.forEach((obj, ind) => {
        minhaArray.push(Number.parseInt(obj.total));
      });
      this.valuesWeek = minhaArray;

      // this.datasets[0] = [
      //   this.valuesWeek,
      //   this.dataDays,
      //   [10, 456, 756, 154, 745, 520, 325, 268, 754, 695, 439, 602]
      // ];
      this.datasets[0] = this.valuesWeek;
      this.updateOptions(0);
    });
    this.dashBoardService.resultDataMonthDashboard.subscribe(dataMonth => {
      // console.log(dataMonth);
      const size = dataMonth.length;
      this.days = [];
      this.dataDays = [];
      for (let i = 0 ; i < size; i++) {
        this.days.push(dataMonth[i].day_.toString());
        this.dataDays.push(dataMonth[i].total);
      }
      this.dataLabels[1] = this.days;

      this.datasets[1] = this.dataDays;
      this.updateOptions(1);
    });

    this.dashBoardService.resultDataYearlyDashboard.subscribe( dataYearly => {
      // console.log(dataYearly);
      const size = dataYearly.length;
      this.dataDays = [];
      for (let i = 0 ; i < size; i++) {
        this.dataDays.push(dataYearly[i].total);
      }

      this.datasets[2] = this.dataDays;
      this.updateOptions(2);
    })
    this.dashBoardService.getDataToDashBoard();
    this.dashBoardService.getDataWeekToDashBoard();
    // this.dashBoardService.getDataMonthToDashBoard();
  }

  ngOnInit() {
    this.buildGraphic();

  }

  public filterGraphic(index: number): void { console.log(index);
    switch (index) {
      case 0:
          this.dashBoardService.getDataWeekToDashBoard();
        break;
      case 1:
          this.dashBoardService.getDataMonthToDashBoard();
        break;
      case 2:
          this.dashBoardService.getDataYearlyToDashBoard();
        break;
    }
  }

  public updateOptions(index: number) { console.log(this.datasets[index]);
    this.salesChart.data.datasets[0].data = this.datasets[index];
    this.salesChart.data.labels = this.dataLabels[index];
    this.salesChart.update();
  }

  filterGraphicBtn(idElement: string) {console.log(idElement);
    // $('#' + idElement).addClass('active');
    // $('a:not(#' + idElement + ')').removeClass('active');
  }

  buildGraphic(): void {

    this.datasets = [
      this.valuesWeek,
      this.dataDays,
      [10, 456, 756, 154, 745, 520, 325, 268, 754, 695, 439, 602]
    ];

    this.dataLabels = [
      ['Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
      this.days,
      ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dec']
    ];

    this.dataNew = this.datasets[0];

    // const chartOrders = document.getElementById('chart-orders');
    // console.log(chartOrders);
    parseOptions(Chart, chartOptions());
    //
    //
    // const ordersChart = new Chart(chartOrders, {
    //   type: 'bar',
    //   options: chartExample2.options,
    //   data: chartExample2.data
    // });

    const chartSales = document.getElementById('chart-sales');

    this.salesChart = new Chart(chartSales, {
      type: 'line',
      options: chartExample1.options,
      data: {
        labels: this.dataLabels[0],
        datasets: [{
          label: 'Performance',
          data: this.datasets[0],
        }],
      }
    });
  }

}
