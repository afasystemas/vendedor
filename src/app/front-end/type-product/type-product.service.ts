import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TypeProductService {


  private urlProduct: String = environment.apiUrl + 'product';
  idOperator = '1';
  listProducEvent: EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient,
              private routes: Router) { }

  public getListProducts(keyword: string) {
    this.http.get(this.urlProduct + '/list/' + this.idOperator + '/' + keyword ).subscribe(data => {
      this.listProducEvent.emit(data);
    });
  }
}
