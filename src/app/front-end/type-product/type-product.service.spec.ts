import { TestBed, inject } from '@angular/core/testing';

import { TypeProductService } from './type-product.service';

describe('TypeProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TypeProductService]
    });
  });

  it('should be created', inject([TypeProductService], (service: TypeProductService) => {
    expect(service).toBeTruthy();
  }));
});
