import { Component, OnInit } from '@angular/core';
import {Product} from '../../entity/Product';
import {LinesServiceService} from '../lines/lines-service.service';
import {Router} from '@angular/router';
import {NewOrderService} from '../new-order/new-order.service';
import {TypeProductService} from './type-product.service';
import {DemandProduct} from '../../entity/DemandProduct';
import {Demand} from '../../entity/Demand';

@Component({
  selector: 'app-type-product',
  templateUrl: './type-product.component.html',
  styleUrls: ['./type-product.component.css']
})
export class TypeProductComponent implements OnInit {

  config = {
    placeholder: 'Selecione um Produto'
  };
  listProducts: Array<Product>;
  listProductsName: Array<string>;
  listDemandProductsView: Array<DemandProduct>;
  ListDemandProduct: Array<DemandProduct>;
  qtdProduct: number;
  totalProducts: number;
  totalWithDeduction: number;
  deductionTotal: number;
  totalValueBackup: number;
  constructor(private router: Router, private newOrderService: NewOrderService, private typeProductService: TypeProductService) {
    // console.log(this.newOrderService.phoneLines);
    // console.log(this.newOrderService.ddd);
    const self = this;
    this.listProducts = []; /* list to object of product with datas of database*/
    this.listProductsName = []; /* list product's name to select*/
    this.listDemandProductsView = []; /* list products choosed*/
    this.totalWithDeduction = 0; /* list products choosed*/
    this.deductionTotal = 0; /* list products choosed*/
    this.totalProducts = 0; /* list products choosed*/
    this.totalValueBackup = this.newOrderService.totalValueDemand; /* list products choosed*/
    console.log(this.totalValueBackup);
    this.typeProductService.listProducEvent.subscribe((data) => {
      this.listProducts = data;
      data.forEach(function (value, index) {
        self.listProductsName.push(value.name);
      });
    });
  }

  ngOnInit() {
    console.log(this.newOrderService.newDemand);
    console.log(this.newOrderService.listDemandPhone);
    const self = this;
     if (this.newOrderService.listDemandProducts.length > 0) {
       this.newOrderService.listDemandProducts.forEach(function (obj, ind) {
         self.listDemandProductsView. push(obj);
       });
     }
  }

  addProduct() {
    const demandProduct = new DemandProduct();
    this.listDemandProductsView.push(demandProduct);
    this.newOrderService.listDemandProducts.push(demandProduct);
  }
  removeProductFromList(ind) {
    this.listProducts = this.listProducts.slice(0, -1);
    this.listDemandProductsView.splice(ind, 1);
    this.newOrderService.listDemandProducts.splice(ind, 1);
    this.calculationTotalByProduct(ind);
    this.newOrderService.products.emit(this.listDemandProductsView);
    // this.reduceTotal()
    // console.log(this.newOrderService.listDemandProducts);
  }
  nextPass() {
    // this.newOrderService.listChooseTypeSale.emit(this.listChooseTypeSales);
    // this.router.navigate(['/logged/nova-venda/servicos']);
    console.log(this.newOrderService.totalValueDemand);
  }


  sumTotal() {
    let valueTotalProducts = 0;

    this.listDemandProductsView.forEach(function (obj, ind) {
      valueTotalProducts = valueTotalProducts + obj.total;
    }); console.log(valueTotalProducts);
    this.newOrderService.products.emit(this.listDemandProductsView);
  }

 // reduceTotal(valueProduct) {
 //    let valueTotalProducts = 0;
 //
 //    this.listDemandProductsView.forEach(function (obj, ind) {
 //      valueTotalProducts = Number.parseFloat(valueTotalProducts.toString()) +
 //        Number.parseFloat(obj.total.toString());
 //    }); console.log(valueTotalProducts);
 //    // this.newOrderService.reduceTotal(valueProduct);
 //  }

  searchProduct(keyword: string) {
    this.typeProductService.getListProducts(keyword);
  }

  settingProduct(product, index) {
    const productChoosed = this.listProducts.find((obj) => obj['name'] === product);
    this.listDemandProductsView[index] = new DemandProduct();
    this.listDemandProductsView[index].product = productChoosed;
    this.listDemandProductsView[index].total =
      (this.listDemandProductsView[index].product.value *
        this.listDemandProductsView[index].qtd) - this.listDemandProductsView[index].deduction;
    this.listProductsName = [];
    this.newOrderService.listDemandProducts[index] = this.listDemandProductsView[index];
    // console.log(this.newOrderService.listDemandProducts);
    // console.log(product);
    // console.log(this.listProducts);
    // console.log(this.listProducts.find((obj) => obj['name'] === product));
    this.sumTotal();
    // console.log(Number.parseFloat(this.totalValueBackup.toString())
    //   + Number.parseFloat(this.newOrderService.listDemandProducts[index].total.toString()));

    // console.log(this.listProducts.find((obj) => obj['name'] === product));
  }

  calculationTotalByProduct(index: number) {
    // if (index !== 0 ) {
    // let v = Number.parseFloat(((this.listDemandProductsView[index].product.value *
    //   this.listDemandProductsView[index].qtd) - this.listDemandProductsView[index].deduction).toFixed(2));
    // console.log(Number.parseFloat(v.toFixed(2)));

    let totalWithDeduction = 0;
    let totalOutDeduct = 0;
    let totalDeduct = 0;
    console.log(this.listDemandProductsView[index]);
      // this.reduceTotal();
    if (this.listDemandProductsView.length > 0) {
      this.listDemandProductsView.forEach(function (obj, ind) {
        totalWithDeduction = totalWithDeduction + (obj.product.value * obj.qtd) - obj.deduction;
        totalOutDeduct = totalOutDeduct + (obj.product.value * obj.qtd);
        totalDeduct =  totalDeduct + obj.deduction;
      });
      this.listDemandProductsView[index].total =
          Number.parseFloat(((this.listDemandProductsView[index].product.value *
            this.listDemandProductsView[index].qtd) -
            this.listDemandProductsView[index].deduction).toFixed(2));
      // this.sumTotal();
    }
    this.totalProducts = totalOutDeduct;
    this.totalWithDeduction = totalWithDeduction;
    this.deductionTotal = totalDeduct;
    this.newOrderService.products.emit(this.listDemandProductsView);
      // } else {
    //   /* validation to that user not setting qtd or point undefined or nothing*/
    //   this.listDemandProductsView[index].qtd = 1;
    //   this.listDemandProductsView[index].deduction = 0;
    // }
  }
}
