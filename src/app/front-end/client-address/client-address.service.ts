import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {User} from '../../entity/User';
import {environment} from '../../../environments/environment.prod';
import {Phone} from '../../entity/Phone';
import {NewOrderService} from '../new-order/new-order.service';

@Injectable({
  providedIn: 'root'
})
export class ClientAddressService {

  url: String = environment.apiUrl + 'demand/consultacnpj/';
  urlDemand: String = environment.apiUrl + 'demand';
  urlState: String = environment.apiUrl + 'state/';
  urlCity: String = environment.apiUrl + 'city/';
  urlneighborhood: String = environment.apiUrl + 'neighborhood/';
  urlstreet: String = environment.apiUrl + 'street/';
  urlServiceState: String = environment.apiUrl + 'state';
  urlServiceCity: String = environment.apiUrl + 'city';
  urlServiceOperator: String = environment.apiUrl + 'operator';
  urlResponsibility: String = environment.apiUrl + 'responsibility';

  companyClient: EventEmitter<any> = new EventEmitter<any>();
  stateEvent: EventEmitter<any> = new EventEmitter<any>();
  addressEvent: EventEmitter<any> = new EventEmitter<any>();
  neighborhoodEvent: EventEmitter<any> = new EventEmitter<any>();
  streetEvent: EventEmitter<any> = new EventEmitter<any>();
  states: EventEmitter<any> = new EventEmitter<any>();
  cities: EventEmitter<any> = new EventEmitter<any>();
  operators: EventEmitter<any> = new EventEmitter<any>();
  responsibilityEvent: EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient, private routes: Router) {}

  public getAddresCompanyByCnpj(cnpj: string): any {
    // const body = JSON.stringify(user);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json');
    // const options = {
    //   headers: headers
    // };
    this.http.get(this.url + cnpj ).subscribe(data => {
      // const dat =  JSON.parse(JSON.stringify(data));
      // console.log(data);
      // console.log(dat['atividade_principal']);
      // console.log(dat.nome);

      // this.companyClient.emit(data);
      // @ts-ignore
      if (data.status === 'ERROR') {console.log(data);
        this.companyClient.emit(data);

      } else {
        // const keywordSearch = this.clientDatas.logradouro + ', ' + this.clientDatas.numero + this.clientDatas.municipio;
        // this.newOrderService.clientDatasReceita = data;
        const datas = {
          // @ts-ignore
          cnpj: data.cnpj,
          // @ts-ignore
          razaosocial: data.nome,
          // @ts-ignore
          logradouro: data.logradouro,
          // @ts-ignore
          cep: data.cep,
          // @ts-ignore
          bairro: data.bairro,
          // @ts-ignore
          numero: data.numero,
          // @ts-ignore
          uf: data.uf,
          // @ts-ignore
          municipio: data.municipio,
          // @ts-ignore
          atividade1: data.atividade_principal[0].text,
          // @ts-ignore
          cnae1: data.atividade_principal[0].code,
          // @ts-ignore
          fantasia: data.fantasia
        };

        // @ts-ignore
        if (data.qsa) {
          // @ts-ignore
          datas.clientName = data.qsa[0].nome;
          // @ts-ignore
          datas.cargo = data.qsa[0].qual;
        }
        // @ts-ignore
        if (data.telefone) {
        // @ts-ignore
          datas.telefone = data.telefone;
        }
        this.companyClient.emit(datas);

        // @ts-ignore
        // this.newOrderService.newDemand.company.address.cep.cep = Number.parseInt(data.cep.replace('-', '').replace('.', ''));
        // @ts-ignore
        // this.newOrderService.newDemand.company.address.cep.neighborhood.name = data.bairro;
        // // @ts-ignore
        // this.newOrderService.newDemand.company.address.cep.street.name = data.logradouro;
        // // @ts-ignore
        // this.newOrderService.newDemand.company.address.numero = data.numero;
        // // this.newOrderService.companyPhones.push(data);
        //
        // // @ts-ignore
        // if (data.telefone !== undefined) {
        //   // @ts-ignore
        //   const sizefone = data.telefone.length;
        //   if (sizefone > 3) {
        //     // @ts-ignore
        //     const startIndexPhone = data.telefone.indexOf(')');
        //     // @ts-ignore
        //     const startIndexDDD = data.telefone.indexOf('(');
        //     // @ts-ignore
        //     const ddd = data.telefone.substring(startIndexDDD + 1, startIndexPhone);
        //     console.log(ddd);
        //     if (startIndexPhone !== -1) {
        //       // @ts-ignore
        //       const dataPhone = data.telefone.substring(startIndexPhone + 1, sizefone)
        //         .replace('-', '')
        //         .replace('', '');
        //       const newPhone = new Phone();
        //       newPhone.numero = Number.parseInt(dataPhone.trim());
        //       newPhone.ddd.ddd = Number.parseInt(ddd);
        //       this.newOrderService.companyPhones = [];
        //       this.newOrderService.companyPhones.push(newPhone);
        //     }
        //   }
        // }
        // // @ts-ignore
        // this.newOrderService.newDemand.company.email = data.email;
        // // @ts-ignore
        // this.newOrderService.newDemand.company.address.cep.city.name = data.municipio;
        // // @ts-ignore
        // this.newOrderService.newDemand.company.address.cep.city.state.uf = data.uf;

        // console.log(this.newOrderService.newDemand);
      }

    });
  }

  public getOperators() {
    this.http.get(this.urlServiceOperator + '/list' ).subscribe(data => {
      // console.log(data);
      this.operators.emit(data);
    });
  }

  public getState(uf: string) {
    this.http.get(this.urlState + 'byuf/' + uf).subscribe(data => {
      this.stateEvent.emit(data);
    });
  }
  public getListState() {
    this.http.get(this.urlServiceState + '/list' ).subscribe(data => {
      // console.log(data);
      this.states.emit(data);
    });
  }
  public getListCity(idState: number) {
    this.http.get(this.urlServiceCity + '/list/' + idState ).subscribe(data => {
      // console.log(data);
      this.cities.emit(data);
    });
  }

  public getResponsibility() {
    this.http.get(this.urlResponsibility + '/list').subscribe(data => {
      // console.log(data);
      this.responsibilityEvent.emit(data);
    });
  }

}
