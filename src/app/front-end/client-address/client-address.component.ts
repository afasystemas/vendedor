import {Component, Input, ViewChild,
  NgZone, OnInit, OnDestroy, ViewEncapsulation, TemplateRef} from '@angular/core';
// import { MapsAPILoader, AgmMap } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';
import {GooglePlaceDirective} from 'ngx-google-places-autocomplete';
import {Address} from 'ngx-google-places-autocomplete/objects/address';
import $ from 'jquery';

import {ClientAddressService} from './client-address.service';
import {Router} from '@angular/router';
import {NewOrderService} from '../new-order/new-order.service';
import {LoggedService} from '../logged/logged.service';
import {State} from '../../entity/State';
import {City} from '../../entity/City';
import {Demand} from '../../entity/Demand';
import {EventEmitter} from '@angular/core';
import {Operator} from '../../entity/Operator';
import {Phone} from '../../entity/Phone';
import {Person} from '../../entity/Person';
import {Responsibility} from '../../entity/Responsibility';
import {Company} from '../../entity/Company';
import {OrderStatus} from '../../entity/OrderStatus';
import {StatusSale} from '../../entity/StatusSale';
import {TypeSale} from '../../entity/TypeSale';
import {AuthorizedAgent} from '../../entity/AuthorizedAgent';
import {Salesman} from '../../entity/Salesman';

declare var google: any;

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
}

@Component({
  selector: 'app-client-address',
  templateUrl: './client-address.component.html',
  styleUrls: ['./client-address.component.css'],
  encapsulation: ViewEncapsulation.None /* habilita sobrescrever o css de component emcapsulados na view*/
})
export class ClientAddressComponent implements OnInit , OnDestroy {

  // @ViewChild('placesRef') placesRef: GooglePlaceDirective;
  options = [{ types: [], componentRestrictions: { country: 'BR' }
          }];
  // geocoder: any;
  // public location: Location = {
  //   lat: -20.4542162,
  //   lng: -54.59548560000002,
  //   marker: {
  //     lat: -20.4542162,
  //     lng: -54.59548560000002,
  //     draggable: true
  //   },
  //   zoom: 10
  // };
  // lat: number;
  // lng: number;
  address: any;
  addressComplete: string;
  // @ViewChild(AgmMap) map: AgmMap;
  cnpj: number;
  clientDatas: any;
  searchAddres: any;
  timeAlert = false;
  cnpjMask = '00.000.000/0000-00';
  cepMask =  '00000-000';
  telefonefixoMask =  '(00) 0000 - 0000';
  celphoneMask =  '(00) 00000 - 0000';

  state: State;
  listStates: Array<State>;
  statesView: Array<string> ;
  listCities: Array<City>;
  listOperators: Array<Operator>;
  city: City;
  cityName: string;
  citiesView: Array<string>;
  operatorsView: Array<string>;
  telefone: number;
  currentOperator: Operator;
  newDemandView: Demand;
  demandEvent:  EventEmitter<any> = new EventEmitter<any>();
  listResponsibility: Array<Responsibility>;
  socio1: Person;
  contato: Person;
  contatophone: string;
  cargo: String;
  listContacts: Array<Person>;
  phonesContactsCompany: string;
  showbtnAvancar = true;

  constructor(
              private zone: NgZone,
              private wrapper: GoogleMapsAPIWrapper,
              private clientAddressService: ClientAddressService,
              private router: Router,
              private newOrderService: NewOrderService,
              private alert: LoggedService) {
    const self = this;
    this.state = new State();
    this.city = new City();
    this.socio1 = new Person();
    this.contato = new Person();
    this.currentOperator = new Operator();
    this.statesView = new Array<string>();
    this.citiesView = new Array<string>();
    this.newDemandView = new Demand();

    this.operatorsView = new Array<string>();

    this.clientDatas = {};
    // this.mapsApiLoader = mapsApiLoader;
    this.zone = zone;
    this.wrapper = wrapper;
    // this.mapsApiLoader.load().then(() => {
    //   this.geocoder = new google.maps.Geocoder();
    // });

    this.clientAddressService.responsibilityEvent.subscribe(data => {
      this.listResponsibility = data;
      this.contato.responsibility = this.listResponsibility.find(obj => obj.name === 'Contato');
    });

    this.addressComplete = this.newOrderService.addressComplete;
    this.cnpj = this.newOrderService.cnpj;
    this.address = this.newOrderService.addressCompleteShow;
    this.demandEvent.subscribe(data => {
      this.newDemandView = data;
    });

    this.clientAddressService.operators.subscribe((data) => {
      this.listOperators = data;
      data.forEach(function (value, key) {
        self.operatorsView.push(value.name);
      });

      if (this.newOrderService.newDemand.id !== 0) {
        this.currentOperator = this.listOperators.find( op =>
          op.id === this.newOrderService.newDemand.fromOperator
        );
      }
    });
    this.clientAddressService.stateEvent.subscribe(data => {

      this.newOrderService.newDemand.company.address.cep.city.state = data;
      // this.clientAddressService.getCityFromState(data.id, data.municipio);
      // console.log(this.newOrderService.newDemand);
    });
    this.clientAddressService.states.subscribe((data) => {
      this.listStates = data;
      data.forEach(function (value, key) {
        self.statesView.push(value.uf);
      });

      if (this.state.id !== 0) {
        this.getCities(this.state.uf);
      }

      if (this.newOrderService.newDemand.id !== 0) {
        this.state = this.listStates.find(obj => obj.uf === this.newOrderService.newDemand.company.address.cep.city.state.uf);
      }
    });
    this.clientAddressService.cities.subscribe((data) => {

      this.citiesView = [];
      this.listCities = data;
      data.forEach(function (value, key) {
        self.citiesView.push(value.name);
      });
      if (this.cityName !== undefined) {
        this.cityName = this.removerAcentos(this.cityName.toLowerCase());
        this.city = this.listCities.find(obj => this.removerAcentos(obj.name.toLowerCase()) === this.cityName);

        this.newOrderService.newDemand.company.address.cep.city = this.city;
      }

    });

    this.newOrderService.demandEvent.subscribe(data => {
      const num = Number.parseInt(
        data.company.cnpj
        .replace('.', '')
        .replace('.', '')
        .replace('/', '')
        .replace('-', ''));
      this.cnpj = num;
      this.newDemandView = data;
      if (this.newOrderService.newDemand.id !== 0 ) {
        this.showbtnAvancar = false;
      } else if (this.newOrderService.newDemand.id === 0 ) {
          this.showbtnAvancar = true;
      }
    });

    this.clientAddressService.companyClient.subscribe((data) => {
      if (data.status === 'ERROR') {
        this.alert.showAlertEvent.emit('Retorno da Receita como CNPJ inválido');
        setTimeout(() => {
            this.alert.showAlertEvent.emit(false);
          },
          5000);

      } else {

        this.newOrderService.newDemand.company.cnpj = data.cnpj.replace('.', '').replace('.', '').replace('/', '').replace('-', '');
        this.newOrderService.newDemand.company.razao_social = data.razaosocial;
        this.newOrderService.newDemand.company.address.cep.street.name = data.logradouro;
        if (data.numero !== 'SN') {
          this.newOrderService.newDemand.company.address.numero = data.numero;
        }
        this.newOrderService.newDemand.company.address.cep.cep = Number.parseInt(data.cep.replace('-', '').replace('.', ''));
        this.newOrderService.newDemand.company.address.cep.neighborhood.name = data.bairro;
        this.newOrderService.newDemand.company.fantasia = data.fantasia;
        this.newOrderService.newDemand.company.cnae_primario = data.cnae1;
        this.newOrderService.newDemand.company.cnaeprimario_descricao = data.atividade1;

        this.socio1 = new Person();
        this.socio1.name = data.clientName;
        this.socio1.responsibility.name = data.cargo
            .substr(data.cargo.indexOf('-') + 1, data.cargo.toString().length).trim();

        this.state  = this.listStates.find(obj => obj.uf === data.uf);
        this.newOrderService.newDemand.company.address.cep.city.state = this.state;

        this.getCities(this.state.uf);
        // this.newOrderService.newDemand.company.address.cep.city.state =
        this.cityName = data.municipio;
        this.newOrderService.cnpjEvent.emit(Number.parseInt(
          data.cnpj.replace('.', '')
            .replace('.', '')
            .replace('/', '')
            .replace('-', '')));
        this.newOrderService.razaosocialEvent.emit(data.razaosocial);
        this.demandEvent.emit(this.newOrderService.newDemand);



        let pcArray = data.telefone.split('/');
        const arrayTrim = [];
        pcArray.forEach(function (obj, ind) {
          arrayTrim[ind] = obj
            .replace(' ', '')
            .replace(' ', '')
            .trim();
        });
        pcArray = arrayTrim;
        pcArray = this.removePhoneRepeated(pcArray);



        const sizephones = pcArray.length;
        this.newOrderService.listPhoneContactsCompany = [];
        for (let i  = 0 ;  i < sizephones; i++) {
          this.newOrderService.listPhoneContactsCompany.push(this.stringToPhone(pcArray[i].trim()));
        }

        this.phonesContactsCompany = pcArray.toString();
      }
    });
    this.newOrderService.destroyerEvent.subscribe(data => {
      if (data) {
        this.cleanDataFromDemand();
      }
    });

    this.clientAddressService.getListState();
    this.clientAddressService.getOperators();
    this.clientAddressService.getResponsibility();

  }

  ngOnInit() {
    // console.log(this.newOrderService.newDemand);
    this.demandEvent.emit(this.newOrderService.newDemand);

    if (this.newOrderService.newDemand.company.id !== 0) {
      this.state = this.newOrderService.newDemand.company.address.cep.city.state;
      this.city = this.newOrderService.newDemand.company.address.cep.city;
    }

    if (this.newOrderService.membercompany.id !== 0) {
      this.socio1 = this.newOrderService.membercompany;
    }
    if (this.newOrderService.contactCompany.id !== 0) {
      this.contato = this.newOrderService.contactCompany;
      this.contatophone = this.newOrderService.contactCompany.phone.ddd.ddd.toString() +
        this.newOrderService.contactCompany.phone.numero.toString();
    }
    if (this.newOrderService.phonesContactsCompanyList !== '') {
      this.phonesContactsCompany = this.newOrderService.phonesContactsCompanyList ;
    }

    if (this.newOrderService.newDemand.id !== 0 ) {
      this.showbtnAvancar = false;
    } else if (this.newOrderService.newDemand.id === 0 ) {
      this.showbtnAvancar = true;
    }
  }

  ngOnDestroy(): void {}

  searchCnpj() {

    this.newOrderService.newDemand = new Demand();
    this.newDemandView = new Demand();
    this.newOrderService.cnpjEvent.emit('');
    this.newOrderService.razaosocialEvent.emit('');

    if (this.cnpj !== undefined && this.cnpj !== null && this.cnpj.toString().length >= 13) {
      this.newOrderService.cnpjEvent.emit(this.cnpj);
      this.newOrderService.newDemand.company.cnpj = this.cnpj.toString();
      this.clientAddressService.getAddresCompanyByCnpj(this.cnpj.toString());
      /*18058323000100*/
    } else {
      let mgs = '';
      if (this.cnpj === undefined ) {
        mgs =  'O CNPJ não pode estar vazio!';
      } else {
        mgs = 'Insira o CNPJ corretamente';
      }
      this.alert.showAlertEvent.emit(mgs);
      setTimeout(() => {
          this.alert.showAlertEvent.emit(false);
        },
        5000);
    }


  }

  getCities(stateChoose: string ): void {
    this.clientAddressService.getListCity(this.listStates.find((obj) => obj.uf === stateChoose).id);
  }
  selectOperator(operatorChoose: string ): void {
    this.currentOperator = this.listOperators.find(obj => obj.name === operatorChoose);
    this.newDemandView.fromOperator = this.currentOperator.id;
    this.newOrderService.newDemand.fromOperator = this.currentOperator.id;
  }

  async nextPass() {
    // this.newOrderService.listChooseTypeSale.emit(this.listChooseTypeSales);
// console.log(this.newOrderService.newDemand);
    // this.newOrderService.newDemand.company.razao_social = this.clientDatas.nome;
    // this.newOrderService.newDemand.company.razao_social = this.clientDatas.nome;
    // this.newOrderService.newDemand.agent =

    this.newOrderService.listContacts = new Array<Person>();
    this.newOrderService.listContacts.push(this.socio1);
    this.newOrderService.listContacts.push(this.contato);

    if (this.newOrderService.newDemand.company.cnpj) {
      if (this.telefone) {
        const newPhone = new Phone();
        newPhone.ddd.ddd = Number.parseInt(this.telefone.toString().substr(0, 2));
        newPhone.numero = Number.parseInt(this.telefone.toString()
        .substr(2, this.telefone.toString().length));
        const indexPhone = this.newOrderService.listPhoneContactsCompany.
        findIndex(obj => obj.numero === newPhone.numero);

        if (indexPhone === -1) {
          this.newOrderService.listPhoneContactsCompany.push(newPhone);
        }
      }
      if (this.contatophone) {
        const newPhone = new Phone();
        newPhone.ddd.ddd = Number.parseInt(this.contatophone.toString().substr(0, 2));
        newPhone.numero = Number.parseInt(this.contatophone.toString()
        .substr(2, this.contatophone.toString().length));
        this.contato.phone = newPhone;
      }
      await this.newOrderService.saveOpportunity();
    } else {
      window.scroll(0, 0);
      this.alert.showAlertEvent.emit('Preencha os dados corretamente');
      setTimeout(() => {
          this.alert.showAlertEvent.emit(false);
        },
        5000);
    }

  }

   removerAcentos( newStringComAcento ) {
    let stringManipulation = newStringComAcento;
    const mapaAcentosHex 	= {
      a : /[\xE0-\xE6]/g,
      A : /[\xC0-\xC6]/g,
      e : /[\xE8-\xEB]/g,
      E : /[\xC8-\xCB]/g,
      i : /[\xEC-\xEF]/g,
      I : /[\xCC-\xCF]/g,
      o : /[\xF2-\xF6]/g,
      O : /[\xD2-\xD6]/g,
      u : /[\xF9-\xFC]/g,
      U : /[\xD9-\xDC]/g,
      c : /\xE7/g,
      C : /\xC7/g,
      n : /\xF1/g,
      N : /\xD1/g,
    };

     // tslint:disable-next-line:forin
     for ( const letter in mapaAcentosHex ) {
      const expressaoRegular = mapaAcentosHex[letter];
      stringManipulation = stringManipulation.replace( expressaoRegular, letter);
    }

    return stringManipulation;
  }

  cleanDataFromDemand(): void {
    this.newOrderService.newDemand = new Demand();
    this.demandEvent.emit(this.newOrderService.newDemand);
    this.newOrderService.cnpjEvent.emit('');
    this.newOrderService.razaosocialEvent.emit('');
    this.citiesView = [];
    this.state = new State();
    this.city = new City();
    this.cnpj = null;
    this.newOrderService.companyPhones = [];
    this.contato = new Person();
    this.socio1 = new Person();
    this.currentOperator = new Operator();
    this.newDemandView = new Demand();
    this.listCities = new Array<City>();
    this.state = new State();
    this.listContacts = new Array<Person>();
    this.contatophone = null;
    this.telefone = null;
    this.phonesContactsCompany = null;
  }
  stringToPhone(stringphone: string): Phone {
    const ddd = stringphone
      .substr(1, 3)
      .replace('(', '')
      .replace(')', '')
      .trim();

    const phone = stringphone
      .substr(4, stringphone.length)
      .replace('-', '')
      .trim();

    const phoneNew = new Phone();
    phoneNew.ddd.ddd = Number.parseInt(ddd);
    phoneNew.numero = Number.parseInt(phone);
    return phoneNew;
  }

  removePhoneRepeated(phonesArray): Array<string> {
    const filterPhones = [];
    const sizrArrray = phonesArray.length;
    for (let i = 0 ; i < sizrArrray; i++) {
      const index = filterPhones.indexOf(phonesArray[i]);
      if (index === -1) {
        filterPhones.push(phonesArray[i]);
      }
    }
    return filterPhones;
  }

}
