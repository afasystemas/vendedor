import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {ignoreElements} from 'rxjs/operators';
import $ from 'jquery';
import {TypeSale} from '../../entity/TypeSale';
import {LinesServiceService} from './lines-service.service';
import {DDD} from '../../entity/DDD';
import {NewOrderService} from '../new-order/new-order.service';
import {Router} from '@angular/router';
import {PlanPackage} from '../../entity/PlanPackage';
import {Phone} from '../../entity/Phone';
import {DemandPhone} from '../../entity/DemandPhone';
import {Package} from '../../entity/Package';
import {Service} from '../../entity/Service';
import {ServicesSaleService} from '../servicesSale/services-sale.service';
import {TitlePlan} from '../../entity/TitlePlan';
import {ModalComponent} from '../modal/modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PlanService} from '../../entity/PlanService';
import {ServiceDemandPhone} from '../../entity/ServiceDemandPhone';

@Component({
  selector: 'app-lines',
  templateUrl: './lines.component.html',
  styleUrls: ['./lines.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class LinesComponent implements OnInit, AfterViewInit {

  totalLines: number;
  currentPage: number;
  ddd: any;
  changeTotalLines: Boolean;
  beforeTotalLines: number;
  detailsAllLines: Array<string>;
  // viewLines = []; /* array by page, only show*/

  /*lista total para ser manipulada com a view devido a paginação*/
  allLines = []; /* array all content lines*/
  listDDDs: Array<DDD>;
  dddsLines: Array<DDD>;
  listServices: Array<PlanService>;
  totalValueService: number;
  listServiceDemandPhone: Array<ServiceDemandPhone>;

  /*lista para view*/
  listServiceDemandPhonesView: Array<ServiceDemandPhone>; /* array by page, only show*/
  listDemandPhonesView: Array<DemandPhone>; /* array by page, only show*/
  listServicesChoose: Array<PlanService>;
  typesSales: Array<TypeSale>;
  typeSaleDefault = 'Novo';
  viewDDDs: Array<number> = [];
  viewTypesSale: Array<TypeSale>;
  viewTypesSaleChoose: Array<String>;
  viewListPackages: Array<TitlePlan>;
  viewPackagesChoose: Array<string>;
  viewServices: Array<string>;
  viewServicesChoose: Array<Service>;

  maskPhone = '0 0000 0000';
  pagination: number;
  totalByPage = 5;
  totalPages: number;
  page1: number;
  typesSale: TypeSale;
  packagesPlan: Array<PlanPackage>;
  planDemand: string;
  totalValue: number;
  showbtnAvancar: boolean;

  constructor(private lineService: LinesServiceService,
              private router: Router,
              private modalService: NgbModal,
              private newOrderService: NewOrderService,
              private serviceSale: ServicesSaleService) {
    const self = this;
    this.showbtnAvancar = false;
    this.listDemandPhonesView = [];
    this.listServiceDemandPhonesView = [];
    this.listDDDs = [];
    this.listServices = [];
    this.listServicesChoose = [];
    this.totalValue = 0;
    this.changeTotalLines = false;
    this.viewTypesSale = [];
    this.viewTypesSaleChoose = [];
    this.viewListPackages = [];
    this.viewServices = [];
    this.viewPackagesChoose = [];
    this.viewServicesChoose = [];
    this.typesSale = new TypeSale();
    this.lineService.listDDDs.subscribe((data) => {
      this.listDDDs = data;

      this.listDDDs.forEach(function (value, index) {
        self.viewDDDs.push(value.ddd);
      });
    });

    this.lineService.listTypeSale.subscribe((data) => {
      this.typesSales = data;
      this.typesSales.forEach(function (ts, index) {
        self.viewTypesSale.push(ts);
      });
    });
    if (this.newOrderService.listPlanServices.length > 0) {
      // console.log(this.newOrderService.listPlanServices);
      this.listServices  = this.newOrderService.listPlanServices;
      this.listServices.forEach(function (obj, index) {
        // @ts-ignore
        // self.viewServices.push(obj.service.name);
      });
    }
    // this.serviceSale.services.subscribe((data) => {
    //   this.listServices = data;
    //   console.log(data);
    //   this.listServices.forEach(function (obj, index) {
    //     // @ts-ignore
    //     self.viewServices.push(obj.Service.name);
    //   });
    // });

    this.newOrderService.services.subscribe((data) => {
      this.listServices = data;
      console.log(data);
      this.listServices.forEach(function (obj, index) {
        self.viewServices.push(obj.service.name);
      });
    });


    this.lineService.getTypeSales();
    this.lineService.getListDDDs();

    this.currentPage = 1;
    /*variavel apenas para apresentar na view o nome do plano*/
    this.planDemand = this.newOrderService.plan.name;
    this.serviceSale.services.subscribe((data) => {
      this.viewServices = data.name;
    });

    this.totalValueService = 0;
  }

  ngOnInit(): void {
    if (this.newOrderService.listTitlePlans !== undefined) {
      console.log(this.newOrderService.listTitlePlans);
      const self = this;
      this.newOrderService.listTitlePlans.forEach( function (obj, val) {
        self.viewListPackages.push(obj);
        console.log(obj.title);
      });
    }
  }
  ngAfterViewInit(): void {  }
  generateRowLines() {
    if (this.totalLines !== undefined && this.ddd !== undefined) {
      // for (let i = 0; i < this.page1; i++) {
      for (let i = 0; i < this.totalLines; i++) {
        this.listServiceDemandPhonesView.push(new ServiceDemandPhone());
      }
    }
  }

  settingDDDAllLinnes() {
    this.generateRowLines();
    const self = this;
    this.listServiceDemandPhonesView.forEach(function (value, ind) {
      if (value.demandPhone.phone.ddd.ddd === undefined) {
        value.demandPhone.phone.ddd = self.listDDDs.find(obj => obj.ddd === self.ddd);
      }
    });
  }

  removeNumberLine(numberRemove: number, index: number) {
    this.listServiceDemandPhonesView.splice(index, 1);
    console.log(this.listServiceDemandPhonesView);
  }

  addNumberLine(numero, indexArray) {
    this.listServiceDemandPhonesView[indexArray].demandPhone.phone.numero = numero;
  }

  settingTypeSaleToLine(typeSale: string, indexArray: number) {}

  settingPackagesToLine(name: string, index: number) {}

  settingService(serviceView: string, indexArray: number) {}

  changePaginate(nextPage: number) {}

  nextPass() {}
}

//   ngOnInit() {
//     if (this.newOrderService.newDemand.id !== 0) {
//       this.showbtnAvancar = true;
//     }
//     this.pagination = 1;
//     this.totalPages = 1;
//
//     if (this.newOrderService.listDemandPhone.length > 0) {
//       this.totalLines = this.newOrderService.listDemandPhone.length;
//       // this.page1 = (this.totalLines > this.totalByPage) ? this.totalByPage : this.totalLines;
//       this.calculatePaginates();
//       // console.log(this.listDemandPhonesView);
//       // console.log(this.newOrderService.listDemandServices);
//       for (let i = 0; i < this.page1; i++) {
//         this.listDemandPhonesView[i] = new DemandPhone();
//         this.listDemandPhonesView[i].phone = this.newOrderService.listDemandPhone[i].phone;
//         this.listDemandPhonesView[i].phone.ddd = this.newOrderService.listDemandPhone[i].phone.ddd;
//         // this.dddsLines.push(this.newOrderService.listDemandPhone[i].phone.ddd);
//         this.listDemandPhonesView[i].typeSale = this.newOrderService.listDemandPhone[i].typeSale;
//         // this.listDemandPhonesView[i].listserviceDemandPhone[i].titleplan = this.newOrderService.listDemandPhone[i].titleplan;
//
//         // this.listDemandPhonesView[i].listserviceDemandPhone[i].planService = this.newOrderService.listDemandPhone[i].planService;
//
//         this.viewTypesSaleChoose.push(this.listDemandPhonesView[i].typeSale.name);
//         // this.viewPackagesChoose.push(this.listDemandPhonesView[i].titleplan.title);
//
//         // this.listDemandPhonesView[i].planService.Services.forEach(obj => {
//         //   this.viewServicesChoose.push(obj.name);
//         // });
//
//         // const totalDemandPhone =
//         //   Number.parseFloat(this.listDemandPhonesView[i].titleplan.valueTotal.toString())
//         //   + Number.parseFloat(this.listDemandPhonesView[i].planService.value.toString());
//         // console.log(i);
//         // console.log(this.listDemandPhonesView[i].totalValue);
//         // console.log(totalDemandPhone);
//         // this.listDemandPhonesView[i].totalValue = totalDemandPhone;
//       }
//       console.log(this.viewServicesChoose);
//     }
//     this.ddd = this.newOrderService.ddd;
//     this.allLines = this.newOrderService.listPhone;
//     this.packagesPlan = this.newOrderService.listPackagesFromPlan;
//     const self = this;
//     // this.packagesPlan.forEach(function (value, ind) {
//     //   self.viewPackages.push(value.Package.name);
//     //
//     // });
//     this.beforeTotalLines = this.totalLines;
//     if (this.newOrderService.listTitlePlans !== undefined) {
//       this.newOrderService.listTitlePlans.forEach( function (obj, val) {
//         self.viewPackages.push(obj.title);
//         // console.log(obj.title);
//       });
//     }
//   }
//
//   ngAfterViewInit(): void {
//
//   }
//
//   async generateRowLines() {
//     // console.log(this.totalLines);
//     // console.log(this.ddd);
//     if (this.ddd !== undefined && this.totalLines !== undefined) {
//       if (this.newOrderService.listDemandPhone.length === 0) {
//         // this.detailsAllLines = [];
//         this.allLines = [];
//         this.listDemandPhonesView = [];
//         this.viewTypesSaleChoose = [];
//         // this.viewLines = [];
//         // this.totalPages = (this.totalLines / this.totalByPage) ^ 0;
//         //
//         // if (this.totalLines % this.totalByPage > 0) { this.totalPages++; }
//         this.calculatePaginates();
//         // this.page1 = (this.totalLines > this.totalByPage) ? this.totalByPage : this.totalLines;
//
//         // this.allLines = new Array(this.totalLines);
//         // this.viewLines = new Array(this.totalLines);
//
//         /*mostrar o nome do plano caso esteja definido*/
//         // if (this.newOrderService.plan.name !== undefined) {
//         //   this.newOrderService.totalValueEvent.emit(this.newOrderService.plan.valueTotal * this.totalLines);
//         // }
//
//           /*preenchendo todas as linhas na this.newOrderService.listDemandPhone*/
//           for (let i = 0; i < this.totalLines; i++) {console.log('teste 123');
//             // this.allLines .push(new Phone());
//             // this.newOrderService.listPhone.push(new Phone());
//             const dem = new DemandPhone();
//             dem.phone.ddd.ddd = this.ddd;
//             console.log(this.ddd);
//             this.newOrderService.listDemandPhone.push(dem);
//
//             // this.newOrderService.listDemandPhone[i].phone = new Phone();
//             // this.newOrderService.listDemandPhone[i].typeSale = this.typesSales.find( obj => obj.name === this.typeSaleDefault);
//
//             // this.newOrderService.listDemandServices.push(new PlanService());
//             // this.newOrderService.listDemandServices
//           }
//             console.log(this.newOrderService.listDemandPhone);
//
//         // else {
//         //   const sizeListPhones = this.newOrderService.listDemandPhone.length;
//         //   for (let i = 0; i < sizeListPhones; i++) {
//         //     this.allLines.push(this.newOrderService.listDemandPhone[i].phone);
//         //   }
//         // }
//
//         for (let i = 0; i < this.page1; i++) {
//           this.listDemandPhonesView.push(this.newOrderService.listDemandPhone[i]);
//           this.viewTypesSaleChoose.push(this.newOrderService.listDemandPhone[i].typeSale.name);
//           // this.viewPackagesChoose.push(this.newOrderService.listDemandPhone[i].planPackage.Package.name);
//         }
//         // const siz = this.newOrderService.listDemandPhone.length;
//         // for (let i = 0; i < siz; i++ ) {
//         //   this.viewTypesSaleChoose.push(this.typeSaleDefault);
//         // }
//       } else {/* caso mude o total de linhas */
//         /*caso aumente o numero de linhas*/
//         if (this.beforeTotalLines < this.totalLines) {
//           const difTotal = this.totalLines - this.beforeTotalLines;
//           // console.log(difTotal);
//           this.calculatePaginates();
//           const typeSalDefault = this.typesSales.find( obj => obj.name === 'Novo');
//           for (let i = 0; i < difTotal; i++) {
//             this.allLines.push(new Phone());
//             this.newOrderService.listPhone.push(new Phone());
//             const demand = new DemandPhone();
//             demand.typeSale = typeSalDefault;
//             this.newOrderService.listDemandPhone.push(demand);
//             this.viewTypesSaleChoose.push(this.typeSaleDefault);
//           }
//           /*caso a mudança de total de linha seja menor que o total de linhas por paginas*/
//           if (this.currentPage === this.totalPages && this.page1 <= this.totalByPage) {
//             /*tamanho atual de linha na view*/
//             const currentSizeLineView = this.listDemandPhonesView.length;
//
//             /*qtd q precisa ser acrescentada na view*/
//              const difAdding = this.totalLines - this.beforeTotalLines;
//             // console.log(difAdding + currentSizeLineView > this.totalByPage);
//             /*se a diferença for maior que o totalByPage
//                 obtem o valor que é necessário para chegar no totalByPage
//                 calculo/lógica apenas para acrescentar na View
//                 */
//             const restLin = (difAdding + currentSizeLineView > this.totalByPage)
//               ? (this.totalByPage - this.beforeTotalLines) : difAdding;
//             console.log(restLin);
//               for (let i = 0 ; i < restLin; i++) {
//                 const newPhone = new Phone();
//                 newPhone.ddd = this.listDDDs.find(obj => obj.ddd === this.ddd);
//                 const newDemand = new DemandPhone();
//                 newDemand.typeSale = this.typesSales.find( obj => obj.name === this.typeSaleDefault);
//                 newDemand.phone = newPhone;
//                 this.listDemandPhonesView.push(newDemand);
//               }
//           } else {/*caso a mudança de total de linha seja maior que o total de linhas por paginas
//                     e tenha que adicionar apenas a diferença*/
//             const currentSizeLineView = this.listDemandPhonesView.length;
//
//             /*qtd q precisa ser acrescentada*/
//             const difAdding = this.totalLines - this.beforeTotalLines;
//             // console.log(difAdding + currentSizeLineView > this.totalByPage);
//             /*se a diferença for maior que o totalByPage
//                 obtem o valor que é necessário para chegar no totalByPage
//                 calculo/lógica apenas para acrescentar na View
//                 */
//             const restLin = (difAdding + currentSizeLineView > this.totalByPage)
//               ? (this.totalByPage - this.beforeTotalLines) : difAdding;
//             console.log(restLin);
//             for (let i = 0 ; i < restLin; i++) {
//               const newPhone = new Phone();
//               newPhone.ddd = this.listDDDs.find(obj => obj.ddd === this.ddd);
//               const newDemand = new DemandPhone();
//               newDemand.typeSale = this.typesSales.find( obj => obj.name === this.typeSaleDefault);
//               newDemand.phone = newPhone;
//               this.listDemandPhonesView.push(newDemand);
//             }
//
//           }
//
//         } else if (this.beforeTotalLines > this.totalLines) {console.log(' diminuiu o numero de linhas');
//
//           const difBefore =  this.beforeTotalLines - this.totalLines ;
//           console.log(difBefore);
//           for (let i = 0; i < difBefore; i++) {
//           //   console.log(i);
//             console.log(this.currentPage === this.totalPages);
//             console.log(this.currentPage );
//             console.log(this.totalPages);
//
//             const last = this.newOrderService.listDemandPhone.length - 1;
//             this.newOrderService.listDemandPhone.splice(last, 1);
//             this.allLines.splice(last, 1);
//             this.newOrderService.listPhone.splice(last, 1);
//
//             // console.log('currentpage = ' + this.currentPage);
//             // console.log('totalPage = ' + this.totalPages);
//             this.verifyPaginate();
//             // console.log('currentpage = ' + this.currentPage);
//             // console.log('totalPage = ' + this.totalPages);
//           }
//
//
//           const totalRemoveLines = this.listDemandPhonesView.length - this.totalLines;
//           console.log(totalRemoveLines);
//           for (let i = 0 ; i < totalRemoveLines; i++) {
//             const lastPosition = this.listDemandPhonesView.length - 1;
//             // if (this.currentPage === this.totalPages) {
//               this.listDemandPhonesView.splice(lastPosition, 1);
//               this.viewTypesSaleChoose.splice(lastPosition, 1);
//               this.viewPackagesChoose.splice(lastPosition, 1);
//               // this.viewServicesChoose.splice(lastPosition, 1);
//             // }
//
//           }
//         }
//       }
//       this.beforeTotalLines = this.totalLines;
//       // for (let i = 0; i < this.totalLines; i++) {
//       //   this.listServicesChoose.push(new PlanService());
//       // }
//
//       this.settingDDDAllLines();
//     }
//     console.log(this.newOrderService.listDemandPhone);
//   }
//
//   changePaginate(nextPage: number) {
//     $('#page' + nextPage).addClass('active');
//     $('li:not(#page' + nextPage + ')').removeClass('active');
//
//     // const toPage = this.totalByPage * nextPage;
//
//     const startIndexPage =  this.totalByPage * (nextPage - 1 ); /*calculate to index of array*/
//     // const startIndexPage =  ((nextPage - 1) * this.totalByPage);
//     // const ate = (toPage > this.allLines.length) ? this.allLines.length  : toPage ;
//     const ate = ((startIndexPage + this.totalByPage) > this.allLines.length) ? this.allLines.length  : startIndexPage + this.totalByPage ;
//     this.listDemandPhonesView = [];
//     this.viewTypesSaleChoose = [];
//     this.viewPackagesChoose = [];
//     this.viewServicesChoose = [];
//
//     const test = [];
//     console.log(this.newOrderService.listDemandPhone);
//     /*for (let i = startIndexPage; i < ate ; i++) {
//       this.viewPackagesChoose.push(this.newOrderService.listDemandPhone[i].titleplan.title);
//       this.newOrderService.listDemandPhone[i].planService.Services.forEach(obj => {
//         this.viewServicesChoose.push(obj.name);
//       });
//
//       this.viewTypesSaleChoose.push(this.newOrderService.listDemandPhone[i].typeSale.name);
//       test.push(this.newOrderService.listDemandPhone[i]);
//     }*/
//
//     this.listDemandPhonesView = test;
//     this.currentPage = nextPage;
//   }
//
//   nextPass() {
//     // this.newOrderService.listChooseTypeSale.emit(this.listChooseTypeSales);
//     // this.router.navigate(['/logged/nova-venda/aparelhos']);
//     console.log(this.newOrderService.newDemand);
//     console.log(this.newOrderService.listDemandPhone);
//     console.log(this.newOrderService.listDemandServices);
//     // console.log(this.listDemandPhonesView);
//     // console.log(this.viewTypesSale);
//     // console.log(this.viewTypesSaleChoose);
//
//   }
//
//   addNumberLine( numero, indexArray) {
//     const indexView = indexArray;
//     if (this.currentPage > 1 ) {
//       indexArray = indexArray + (this.totalByPage * (this.currentPage - 1));
//     }
//     if ( numero !== '') {console.log(indexArray);
//       // const dddNumber = Number.parseInt(numero.charAt(1) + numero.charAt(2));
//       // const dddObj = this.listDDDs.find(obj => obj.ddd === dddNumber);
//       // console.log(Number.parseInt(numero.charAt(1) + numero.charAt(2)));
//       // console.log(Number.parseInt(numero.slice(4, 16).replace(' ', '').replace(' ', '').replace(' ', '')));
//       const numberPhone = Number.parseInt(numero.replace(' ', '').replace(' ', ''));
//       console.log(numberPhone);
//       // this.newOrderService.listPhone[indexArray].numero = numberPhone;
//       this.newOrderService.listDemandPhone[indexArray].phone.numero = numberPhone;
//
//       console.log(this.newOrderService.listDemandPhone);
//
//     } else {/*se o numero for vazio setar a venda como nova*/
//       const typS = this.typesSales.find( obj => obj.name === this.typeSaleDefault);
//       this.newOrderService.listDemandPhone[indexArray].typeSale = typS;
//       this.newOrderService.listDemandPhone[indexArray].typeSale = typS;
//       this.listDemandPhonesView[indexView].typeSale = typS;
//       this.viewTypesSaleChoose[indexView] = this.typeSaleDefault;
//     }
//   }
//
//   removeNumberLine(numberRemove: number, index: number) {
//     // console.log(this.newOrderService.listPhone);
//     console.log(index);
//     console.log(numberRemove);
//     // const index = this.newOrderService.listPhone.find( obj => obj.number === Number.parseInt($('#' + numberRemove).val()));
//     this.newOrderService.listPhone.splice(this.calculateIndex(index), 1);
//     this.listDemandPhonesView.splice(index, 1);
//     this.allLines.splice(this.calculateIndex(index), 1);
//     this.newOrderService.listDemandPhone.splice(this.calculateIndex(index), 1);
//     this.newOrderService.listDemandServices.splice(this.calculateIndex(index), 1);
//
//
// const phoneRemove: Phone = this.newOrderService.listPhone.find( obj => obj.numero === Number.parseInt($('#' + numberRemove).val()));
//
//             // console.log(this.currentPage);
//             // console.log(this.totalPages);
//     // if (index !== -1) {
//     //     this.newOrderService.phoneLines.splice(index, 1);
//     //   }
//       $('#' + numberRemove).parent().parent().parent().remove();
//       this.totalLines--;
//       this.verifyPaginate();
//         // console.log(this.listDemandPhonesView);
//         // console.log(this.allLines);
//         this.newOrderService.demandPhoneEvent.emit(this.newOrderService.listDemandPhone);
//         console.log(this.newOrderService.listDemandPhone);
//         // console.log(this.newOrderService.listPhone);
//   }
//
//   settingDDD() {
//     this.generateRowLines();
//     const self = this;
//     this.newOrderService.ddd = this.ddd;
//     console.log('SELECIONADO');
//     console.log(this.newOrderService.listDemandPhone);
//     this.newOrderService.listDemandPhone.forEach(function (value, ind) {
//       if (value.phone.ddd.ddd === undefined) {
//         value.phone.ddd = self.listDDDs.find(obj => obj.ddd === self.ddd);
//         self.listDemandPhonesView[ind].phone = value.phone;
//         // console.log();
//       }
//     });
//
//   }
//
//   calculatePaginates() {
//     this.totalPages = (this.totalLines / this.totalByPage) ^ 0;
//
//     if (this.totalLines % this.totalByPage > 0) { this.totalPages++; }
//     this.page1 = (this.totalPages > 1) ? this.totalByPage : this.totalLines;
//   }
//
//   /*index da linha a ser setada com o pacote com nome*/
//   settingPackagesToLine(name: string, index: number) {
//     console.log(this.newOrderService.listDemandServices);
//     this.newOrderService.mgsPackages =  'Definir para todas as Linhas?';
//     // console.log(this.newOrderService.listTitlePlans.find(obj => obj.title === name));
//     // const titleplan: TitlePlan = this.newOrderService.listTitlePlans.find(obj => obj.title === name);
//     // this.settingServiceToALlLines(titleplan);
//     console.log('depois');
//     // this.listDemandPhonesView[index].planPackage = this.packagesPlan.find((obj) => obj['Package'].name === name);
//     /*this.newOrderService.listDemandPhone[this.calculateIndex(index)].titleplan = titleplan;*/
//
//     // this.newOrderService.sumTotal(this.newOrderService.listDemandPhone[this.calculateIndex(index)].titleplan.valueTotal);
//     /*let sumTotalPackages = 0;
//     this.newOrderService.listDemandPhone.forEach(function (obj) {
//       if (obj.titleplan.valueTotal !== 0 ) {
//         sumTotalPackages += Number.parseFloat(Number.parseFloat(obj.titleplan.valueTotal.toString()).toFixed(2));
//       }
//     });*/
//     this.newOrderService.demandPhoneEvent.emit(this.newOrderService.listDemandPhone);
//         // console.log(this.viewPackagesChoose);
//         // console.log(this.viewPackages);
//     // const self = this;
//     // this.totalValue = 0;
//     // let sumTotal = 0;
//     // this.newOrderService.listDemandPhone.forEach(function (obj, ind) {
//     //   sumTotal = Number.parseFloat(sumTotal.toString()) + Number.parseFloat(obj.planPackage.value.toString());
//     // });
//     // this.totalValue = sumTotal;
//     // this.newOrderService.totalValueEvent.emit(this.totalValue);
//
//   }
//
//   settingDDDByLine(dddChoosed: number, index: number) {
//      this.listDemandPhonesView[index].phone.ddd = this.listDDDs.find( obj => obj.ddd === dddChoosed);
//      this.newOrderService.listDemandPhone[this.calculateIndex(index)].phone.ddd =
//        this.listDDDs.find( obj => obj.ddd === dddChoosed);
//   }
//
//   settingDDDAllLines() {
//     const dddChose = this.listDDDs.find( obj => obj.ddd === this.ddd);
//     this.newOrderService.listDemandPhone.forEach(function (valeu, index) {
//       valeu.phone.ddd = dddChose;
//     });
//     console.log(this.newOrderService.listDemandPhone);
//   }
//
//   settingTypeSaleToLine(typeSale: string, indexArray: number) {
//     this.newOrderService.listDemandPhone[this.calculateIndex(indexArray)].typeSale =
//       this.typesSales.find( obj => obj.name === typeSale );
//     $('#typeSale0').prop('disabled', false);
//   }
//
//   /*calculate index to newOrderService.listDemandPhone*/
//   calculateIndex(indexArray) {
//     let indexTarger = 0;
//     if ( this.currentPage > 1) {
//       const startIndex = this.totalByPage * (this.currentPage - 1);
//       indexTarger = startIndex + indexArray;
//     } else {
//       indexTarger = indexArray;
//     }
//     return indexTarger;
//   }
//
//   verifyPaginate() {
//     this.calculatePaginates();
//     /* caso a quantidade de linhas seja menor que a quantidade que contenham dentro do total de paginas*/
//     if (this.currentPage > this.totalPages) {
//       this.currentPage --;
//       this.changePaginate(this.currentPage);
//     }
//   }
//
//   // searchServices(keyword: string) {
//   //     this.serviceSale.getListServiceFilter(keyword);
//   // }
//   settingService(serviceView: string, indexArray: number) {
//     // console.log(this.newOrderService.listDemandPhone);
//     console.log(serviceView);
//     console.log(indexArray);
//     console.log(this.listServices[indexArray]);
//     // const servID = this.listServices[indexArray].service.find(obj => obj.name === service).id;
//
//     // console.log(servID);
//     // const newplanServ = new PlanService();
//     // newplanServ.service = this.listServices[indexArray].service;
//     this.viewServicesChoose.push(this.listServices[indexArray].service);
//     console.log(this.viewServicesChoose[indexArray]);
//     // console.log(this.listServices[indexArray].service);
//
//     // this.newOrderService.listDemandPhone[this.calculateIndex(indexArray)]
//     //   .planService.TitlePlan.listServices.push(this.listServices.find(obj => obj.name === service))
//     // console.log(
//     //   this.newOrderService.listPlanServices.find( ps =>
//     //     ps.Service.id === servID)
//     // );
//     // console.log(this.listServices.find(obj => obj.name === service));
//     // console.log(this.viewServices[indexArray]);
//     // console.log(this.viewServicesChoose);
//     // this.newOrderService.listDemandPhone[this.calculateIndex(indexArray)].planService =
//      // this.newOrderService.listPlanServices.find( ps =>
//      //   ps.Service.id === (this.listServices.find(obj => obj.name === service).id));
//
//     // this.calculateTotalDemandPhone(indexArray);
//
//     //  const planServ = new PlanService();
//     // planServ.service = this.listServices.find(obj => obj.name === service);
//     // this.newOrderService.listDemandServices[this.calculateIndex(indexArray)] =
//       // this.newOrderService.listDemandPhone[this.calculateIndex(indexArray)].planService;
//     // console.log(this.newOrderService.listDemandPhone);
//     this.newOrderService.demandPhoneEvent.emit(this.newOrderService.listDemandPhone);
//
//     console.log(this.newOrderService.listDemandPhone);
//     // console.log(this.newOrderService.listServices);
//     // console.log(this.newOrderService.listDemandServices);
//   }
//   settingServiceToALlLines(titles: TitlePlan) { console.log('Passei por aki');
//     const response = false;
//     const self = this;
//     this.modalService.open(ModalComponent).result.then((result) => {
//       // console.log(typeof result);
//       // console.log(result);
//
//       /*if (result) {
//         this.newOrderService.listDemandPhone.forEach(function (obj, val) {
//           obj.titleplan = titles;
//         });
//         for (let i = 0; i < this.totalLines; i++) {
//           this.viewPackagesChoose[i] = titles.title;
//         }
//         // console.log(this.newOrderService.listDemandPhone);
//       }*/
//         console.log('antes');
//         this.newOrderService.demandPhoneEvent.emit(this.newOrderService.listDemandPhone);
//     }).catch((error) => {
//       console.log(error);
//     });
//   }
//   calculateTotalDemandPhone(indexArray) {
//     /*const totalDemandPhone =
//       Number.parseFloat(this.newOrderService.listDemandPhone[this.calculateIndex(indexArray)].titleplan.valueTotal.toString())
//       + Number.parseFloat(this.newOrderService.listDemandPhone[this.calculateIndex(indexArray)].planService.value.toString());
//     this.newOrderService.listDemandPhone[this.calculateIndex(indexArray)].totalValue = totalDemandPhone;
//   */}
//   removeServiceToLinne(indexLinne: number, indexService: number) {
//     // console.log(this.newOrderService
//     //   .listDemandPhone[this.calculateIndex(indexLinne)]
//     //   .planService.TitlePlan.listServices[indexService]
//     // );
//
//   }
//
// }
