import { TestBed, inject } from '@angular/core/testing';

import { LinesServiceService } from './lines-service.service';

describe('LinesServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LinesServiceService]
    });
  });

  it('should be created', inject([LinesServiceService], (service: LinesServiceService) => {
    expect(service).toBeTruthy();
  }));
});
