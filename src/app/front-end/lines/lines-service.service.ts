import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LinesServiceService {

  urlDDD: String = environment.apiUrl + 'ddd';
  listDDDs: EventEmitter<any> = new EventEmitter<any>();
  urlTypeSale: String = environment.apiUrl + 'typesales';
  listTypeSale: EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient,
              private routes: Router) { }

  public getListDDDs() {
    this.http.get(this.urlDDD + '/list' ).subscribe(data => {
      this.listDDDs.emit(data);
    });
  }

  public getTypeSales() {
    this.http.get(this.urlTypeSale + '/list' ).subscribe(data => {
      this.listTypeSale.emit(data);
    });
  }
}
