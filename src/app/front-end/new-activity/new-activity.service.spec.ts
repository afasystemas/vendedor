import { TestBed, inject } from '@angular/core/testing';

import { NewActivityService } from './new-activity.service';

describe('NewActivityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewActivityService]
    });
  });

  it('should be created', inject([NewActivityService], (service: NewActivityService) => {
    expect(service).toBeTruthy();
  }));
});
