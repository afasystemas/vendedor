import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment.prod';
import {Activity} from '../../entity/Activity';
import {SalesHistoric} from '../../entity/SalesHistoric';

@Injectable({
  providedIn: 'root'
})
export class NewActivityService {

  urlActivity: String = environment.apiUrl + 'activity/';
  urlSaleHistoric: String = environment.apiUrl + 'salehistoric/';
  saleHistoric: EventEmitter<any> = new EventEmitter<any>();
  response: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient,
              private routes: Router) {
  }

  public saveActivityHistoric(activity: Activity) {
    const body = JSON.stringify({activity: activity});
    // const headers = new HttpHeaders().set('Content-Type', 'application/json');
    // const options = {
    //   headers: headers
    // };
    this.http.post(this.urlActivity + 'save', body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
        .set('apikey', environment.KeySecurity),
    }).subscribe(data => {
      // @ts-ignore
      this.response.emit(data.saveSuccess);
    });
  }


  public getHistoric(code: string, status: number): any {
    const salesmanId = localStorage.getItem('userid');
    this.http.get(this.urlSaleHistoric + '/' + status + '/' + code).subscribe(data => {
      // console.log(data);
      // @ts-ignore
      this.saleHistoric.emit(data);
    });
  }

}
