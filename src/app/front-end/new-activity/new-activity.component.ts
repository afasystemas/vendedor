import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import {NewOrderService} from '../new-order/new-order.service';
import {ActivatedRoute} from '@angular/router';
import {Activity} from '../../entity/Activity';
import {NewActivityService} from './new-activity.service';
import {SalesHistoric} from '../../entity/SalesHistoric';
import {LoggedService} from '../logged/logged.service';

@Component({
  selector: 'app-new-activity',
  templateUrl: './new-activity.component.html',
  styleUrls: ['./new-activity.component.css']
})
export class NewActivityComponent implements OnInit {

  showDescription = false;
  showTitle = false;
  salesHistoric: SalesHistoric;
  activity: Activity;
  code: string;
  statussale: number;
  constructor(private route: ActivatedRoute, private newActivityService: NewActivityService,
              serviceSuccess: LoggedService) {
    this.activity = new Activity();
    this.salesHistoric = new SalesHistoric();
    this.newActivityService.saleHistoric.subscribe(data => {
      this.salesHistoric = data;
      this.activity.saleHistoric = this.salesHistoric.id;
    });
    this.route.params.subscribe(params => {
      this.code = params['code']; // (+) converts string 'id' to a number
      this.statussale = +params['statussale']; // (+) converts string 'id' to a number
      this.newActivityService.getHistoric(this.code, this.statussale);
       console.log(this.statussale);

       console.log(this.code);
    });

    this.newActivityService.response.subscribe(data => {
      console.log(data);
      if (data) {
        serviceSuccess.showSuccessEvent.emit('Atualizado com Sucesso!');
        this.cleanForm();
      } else {
        serviceSuccess.showAlertEvent.emit(data);
      }
    });

  }

  ngOnInit() {
    const self = this;
    $( '#description' )
      .focusout(function() {
        if (self.activity.description === '' ) {
          self.showDescription = true;
        } else {
          self.showDescription = false;
        }
      });
    $( '#titulo' )
      .focusout(function() {
        if (self.activity.title === '') {
          self.showTitle = true;
        } else {
          self.showTitle = false;
        }
      });

  }

  saveActivity() {
    this.showTitle = (this.activity.title === '') ? true : false;
    this.showDescription = (this.activity.description === '' ) ? true : false;
    if (!this.showTitle && !this.showDescription) {

      this.newActivityService.saveActivityHistoric(this.activity);
    }
  }

  cleanForm() {
    this.activity = new Activity();
    this.activity.saleHistoric = this.salesHistoric.id;
  }

}
