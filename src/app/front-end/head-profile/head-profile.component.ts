import { Component, OnInit } from '@angular/core';
import {User} from '../../entity/User';
import {LoginService} from '../login/login.service';
import {LoginComponent} from '../login/login.component';
import {environment} from '../../../environments/environment.prod';
import {ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {ROUTES} from '../menu/menu.component';

@Component({
  selector: 'app-head-profile',
  templateUrl: './head-profile.component.html',
  styleUrls: ['./head-profile.component.css']
})
export class HeadProfileComponent implements OnInit {

  public focus;
  public listTitles: any[];

  user: User;
  photoProfile: string  ;
  nameMenuSelected: string;

  constructor(private loginService: LoginService, private router: Router) {
    this.user = new User();
    this.user.name = localStorage.getItem('userName');
    console.log(localStorage.getItem('userName'));
    this.photoProfile = environment.apiUrlImage + localStorage.getItem('userPhoto');
    this.nameMenuSelected = '';
    this.listTitles = ROUTES.filter(listTitle => listTitle);
    this.loginService.user.subscribe(data => {
      console.log(data);
      this.user = data;
      // this.user.name = data.name;
    this.photoProfile = this.user.urlphotoprofile;
    });
    if (localStorage.getItem('user')) {
      this.user.name = localStorage.getItem('userName');
    }

    // this.router.events.subscribe(data => {
    //   console.log(data.url);
    //   this.listTitles.forEach((val, ind) => {
    //     if (val.path === data.url) {
    //       this.nameMenuSelected = val.title;
    //     }
    //   });
    // });
    // console.log(this.user);
    // console.log(loginService.userLogged);
  }

  ngOnInit() {}



}
