import { Component, OnInit } from '@angular/core';
import {Plan} from '../../entity/Plan';
import {PlanService} from './plan.service';
import {NewOrderService} from '../new-order/new-order.service';
import {Router} from '@angular/router';
import $ from 'jquery';
import {Package} from '../../entity/Package';
import {PlanPackage} from '../../entity/PlanPackage';
import {TitlePlan} from '../../entity/TitlePlan';
import {DemandPhone} from '../../entity/DemandPhone';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {

  config = {
   placeholder: 'Selecione um Plano'
  };
  plans: Array<Plan>;
  plansView =  [];
  plan: Plan;
  planChoosed: Plan;
  titlePlanChoosed: TitlePlan;
  listTitlePlans: Array<TitlePlan>;
  packagesPlan: Array<PlanPackage>;
  constructor(private planService: PlanService,
              private newOrderService: NewOrderService,
              private router: Router) {
    this.plans = new Array<Plan>();
    this.plan = new Plan();
    this.planChoosed = new Plan();
    this.listTitlePlans = new Array<TitlePlan>();
    this.planService.getListPlans();
    const self = this;
    this.planService.plans.subscribe((data) => {
      this.plans = data;
      data.forEach(function (value, key) {
        self.plansView.push(value.name);
      });
    });
    this.packagesPlan = [];
    this.planService.packagesFromPlanEvent.subscribe((data) => { console.log(data);
      this.newOrderService.packagesEvent.emit(data);
      this.packagesPlan = data;
      // data.forEach(function (value, ind) {
      //   self.packagesPlan.push(value);
      // });
    });
    this.newOrderService.listTitlesPlanEvent.subscribe((data) => {
      this.listTitlePlans = data;
    });

    if (this.newOrderService.listTitlePlans !== undefined ) {
      this.listTitlePlans = this.newOrderService.listTitlePlans;
    }

    // this.newOrderService.listChooseTypeSale.subscribe((data) => {
    //   console.log(data);
    // });
  }

  ngOnInit() {console.log(this.newOrderService.newDemand);
    // $('#select-plan').
    this.plan = this.newOrderService.plan;
    this.packagesPlan = this.newOrderService.listPackagesFromPlan;
    this.planChoosed = this.newOrderService.plan;
    console.log(this.newOrderService.plan);
    $('#select-plan option[text="' + this.plan.name + '"]').attr('selected', 'selected');
  }

  nextPass() {
    if (this.plans.find((obj) => obj['name'] === this.plan.name) !== undefined) {
    // this.router.navigate(['/logged/nova-venda/linhas']);
    }

  }

  settingPlan(name: string) {
    this.newOrderService.plan = this.plans.find((obj) => obj['name'] === this.plan.name);
    // console.log(this.newOrderService.plan);
    // setar como novo todos os dados do pedido
    this.newOrderService.ddd = undefined;
    this.newOrderService.listDemandServices = [];
    this.newOrderService.demandPhoneEvent.emit(new Array<DemandPhone>());
    this.planChoosed = this.newOrderService.plan;


    this.newOrderService.planEvent.emit(this.planChoosed);
    this.planService.getPackagesByPlan(this.planChoosed.id);
     console.log(this.planChoosed);
  }

}
