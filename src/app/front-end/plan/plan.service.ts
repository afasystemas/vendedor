import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Plan} from '../../entity/Plan';
import {TitlePlan} from '../../entity/TitlePlan';
import {Service} from '../../entity/Service';
import {Package} from '../../entity/Package';
import {Title} from '@angular/platform-browser';
import {NewOrderService} from '../new-order/new-order.service';

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  urlPlan: String = environment.apiUrl + 'plan';
  urltitlePlan: String = environment.apiUrl + 'titleplan';
  plans: EventEmitter<any> = new EventEmitter<any>();
  packagesFromPlanEvent: EventEmitter<any> = new EventEmitter<any>();
  titlePlansEvent: EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient,
              private routes: Router,
              private service: NewOrderService) { }

  public getListPlans() {
    this.http.get(this.urlPlan + '/list' ).subscribe(data => {
      this.plans.emit(data);
    });
  }

  public getPackagesByPlan(planId: number) {
    this.http.get(this.urltitlePlan + '/byplan/' + planId ).subscribe(data => {
      const planServices = Array<PlanService>();
      const servicesAdditional = Array<TitlePlan>();
      const packages = Array<Package>();
      const titles = Array<TitlePlan>();
      console.log(data);
      data[1].forEach(function (obj, index) {
        // @ts-ignore
        servicesAdditional.push(obj.titleplan);
        // console.log(obj);
      });
      data[2].forEach(function (obj, index) {
        titles.push(obj);
      });
      // console.log(titles);
      this.service.listTitlesPlanEvent.emit(titles);
      this.service.services.emit(servicesAdditional);
      // titles.forEach(function (ob, val) {
      //   console.log(ob.title);
      //
      // });
      // this.packagesFromPlanEvent.emit(data);
    });
  }
}
