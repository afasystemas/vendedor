import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FinancialStatementService} from './financial-statement.service';
import {environment} from '../../../environments/environment.prod';


@Component({
  selector: 'app-financial-statement',
  templateUrl: './financial-statement.component.html',
  styleUrls: ['./financial-statement.component.css'],
  encapsulation: ViewEncapsulation.None /* habilita sobrescrever o css de component emcapsulados na view*/
  // encapsulation: ViewEncapsulation.ShadowDom // introduced in Angular 6.1 (Shadow Dom v1)
  // encapsulation: ViewEncapsulation.Native // deprecated in Angular 6.1+ (Shadow Dom v0)
})
export class FinancialStatementComponent implements OnInit {

  imgSearchMoney = environment.apiUrlIcon + 'search-money2.png';
  imgEyesMoney = environment.apiUrlIcon + 'eyes-money.png';
  imgFlyMoney = environment.apiUrlIcon + 'fly-money.png';
  imgHandBagMoney = environment.apiUrlIcon + 'hand-bag-money.png';
  imgGraphMoney = environment.apiUrlIcon + 'graph-money.png';
  valueProporsal: any;
  valueClosed: any;
  valueLost: any;
  valueGain: any;
  valueTotal: any;

  startDate: Date;
  startDateString: string;
  finalDate: Date;
  finalDateString: string;
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd/MM/yyyy',
    defaultOpen: false
  };

  constructor(private financialService: FinancialStatementService) {
    this.valueProporsal = '';
    this.valueClosed = '';
    this.valueLost = '';
    this.valueGain = '';
    this.valueTotal = '';
    this.startDate = new Date();
    this.finalDate = new Date();
    this.startDate.setDate(1);
    this.startDateString = this.startDate.getDate() + '-' + this.startDate.getMonth() + '-' + this.startDate.getFullYear();
    this.finalDateString = this.finalDate.getDate() + '-' + this.finalDate.getMonth() + '-' + this.finalDate.getFullYear();

    this.financialService.getFinacialStatement(this.startDateString, this.finalDateString);
    this.financialService.totalEvent.subscribe(data => {
      this.valueTotal = data;
    });
    this.financialService.proporsalEvent.subscribe(data => {
      this.valueProporsal = data;
    });
    this.financialService.closedEvent.subscribe(data => {
        this.valueClosed = data;
    });
    this.financialService.lostEvent.subscribe(data => {
      this.valueLost = data;
    });
    this.financialService.gainEvent.subscribe(data => {
      this.valueGain = data;
    });

  }
  ngOnInit() {
    // const dataString = this.financialService.transformDate(new Date()).split('/');

    // this.startDate = new Date(this.finalDate.getFullYear(), 4, 1).toLocaleDateString();
  }

  selectDateFinal(event: any) {
    this.finalDate = new Date (event);
    // console.log(new Date (event));

    // this.startDateString = this.startDate.getDate() + '-' + this.startDate.getMonth() + '-' + this.startDate.getFullYear();
    this.finalDateString = this.finalDate.getDate() + '-' + this.finalDate.getMonth() + '-' + this.finalDate.getFullYear();
    console.log(this.finalDateString);
  }
  selectDateStart(event: any) {
    this.startDate = new Date (event);
    this.startDateString = this.startDate.getDate() + '-' + this.startDate.getMonth() + '-' + this.startDate.getFullYear();
    console.log(this.finalDateString);
  }
  redoGetFinancial() {
    this.financialService.getFinacialStatement(this.startDateString, this.finalDateString);
  }

}
