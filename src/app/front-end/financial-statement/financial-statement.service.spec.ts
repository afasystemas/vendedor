import { TestBed, inject } from '@angular/core/testing';

import { FinancialStatementService } from './financial-statement.service';

describe('FinancialStatementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FinancialStatementService]
    });
  });

  it('should be created', inject([FinancialStatementService], (service: FinancialStatementService) => {
    expect(service).toBeTruthy();
  }));
});
