import {EventEmitter, Inject, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {LOCALE_ID} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FinancialStatementService {

  urldemand: String = environment.apiUrl + 'demand';
  idSalesman = localStorage.getItem('userid');

  totalEvent: EventEmitter<any> = new EventEmitter<any>();
  proporsalEvent: EventEmitter<any> = new EventEmitter<any>();
  closedEvent: EventEmitter<any> = new EventEmitter<any>();
  lostEvent: EventEmitter<any> = new EventEmitter<any>();
  gainEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient,
              private routes: Router,
              private datePipe: DatePipe) { }

  public getFinacialStatement(dateStart: string, dateFinal: string) {
    const dateSt = this.StringToDate(dateStart);
    const dateFin = this.StringToDate(dateFinal);
    this.http.get(this.urldemand + '/financial/' + this.idSalesman + '/' +
                  dateSt + '/' + dateFin).subscribe(data => {
      this.totalEvent.emit(data[0][0]);
      this.proporsalEvent.emit(data[1][0]);
      this.closedEvent.emit(data[2][0]);
      this.lostEvent.emit(data[3][0]);
      this.gainEvent.emit(data[4][0]);
      console.log(data);
    });
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  StringToDate(value: string) {
    const date = value.split('-');
    return this.transformDate(new Date(Number.parseInt(date[2]), Number.parseInt(date[1]), Number.parseInt(date[0])));
  }
}
