import {EventEmitter, Injectable} from '@angular/core';
import {User} from '../../entity/User';
import {LoginService} from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedService {

  showAlertEvent: EventEmitter<any> = new EventEmitter<any>();
  showSuccessEvent: EventEmitter<any> = new EventEmitter<any>();
  user: User;
  id = localStorage.getItem('userid');
  constructor(private loginService: LoginService) {
    loginService.user.subscribe(data => {
      this.user = data;
    });
    if (loginService.userLogged) {
      this.user = loginService.userLogged;
    }
    this.user = loginService.userLogged;
    const us = localStorage.getItem('user');
    // @ts-ignore
    console.log(this.id);
  }
}
