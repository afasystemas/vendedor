import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {LoggedService} from './logged.service';
import {NewOrderService} from '../new-order/new-order.service';
import { ngxLoadingAnimationTypes, NgxLoadingComponent } from 'ngx-loading';

@Component({
  selector: 'app-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['./logged.component.css'],
  animations: [
    trigger('containerStatus', [
      state('out', style({
        transform: 'translate3d(0, 0, 0)',
        width: ''
      })),
      state('in', style({
        transform: 'translate3d(10%, 0, 0)',
        width: ''
      })),
    transition('in => out', animate('400ms ease-in-out')),
    transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class LoggedComponent implements OnInit {

  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>;
  // @ViewChild('placesRef') placesRef: GooglePlaceDirective;

  PrimaryWhite = '#ffffff';
  SecondaryGrey = '#ccc';
  PrimaryRed = '#dd0031';
  SecondaryBlue = '#006ddd';

  public primaryColour = this.PrimaryRed;
  public secondaryColour = this.SecondaryBlue;
  public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  public config = {
    animationType: ngxLoadingAnimationTypes.none,
    primaryColour: this.primaryColour,
    secondaryColour: this.secondaryColour,
    tertiaryColour: this.primaryColour,
    backdropBorderRadius: '6px' };
  showAlert = false;
  mgsAlert: string;
  showSuccess = false;
  mgsSuccess: string;
  public loadingTemplate: TemplateRef<any>;
  public loading = false;
  containerState: String = 'in';
  constructor(private serviceLogged: LoggedService, private newOrderService: NewOrderService) {
    this.newOrderService.loadingSaveEvent.subscribe(data => {
      this.loading = data;
    });
    window.scroll(0, 0);
    this.serviceLogged.showAlertEvent.subscribe(data => {
      if (typeof data === 'boolean' && data === false) {
        this.showAlert = data;
        this.mgsAlert = '';
      } else if (typeof data === 'string' && data !== '') {
        this.mgsAlert = data;
        this.showAlert = true;
      this.timeToAlert();
      }
    });
    this.serviceLogged.showSuccessEvent.subscribe(data => {
      if (typeof data === 'boolean' && data === false) {
        this.showSuccess = data;
        this.mgsSuccess = '';
      } else if (typeof data === 'string' && data !== '') {
        this.mgsSuccess = data;
        this.showSuccess = true;
        this.timeToAlert();
      }
    });
  }

  ngOnInit() {
  }
  statusResponsive() {/*console.log('dentro do toogleMneu')*/
    this.containerState = this.containerState === 'out' ? 'in' : 'out';
  }

  private timeToAlert () {
    setTimeout(() => {
        this.showSuccess = false;
        this.showAlert = false;
      },
      5000);
  }

}
