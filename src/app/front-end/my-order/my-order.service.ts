import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Demand} from '../../entity/Demand';

@Injectable({
  providedIn: 'root'
})
export class MyOrderService {

  listDemands: EventEmitter<any> = new EventEmitter<any>();
  listStatusSale: EventEmitter<any> = new EventEmitter<any>();
  updateStatus: EventEmitter<any> = new EventEmitter<any>();
  allResponse: EventEmitter<Boolean> = new EventEmitter<Boolean>();
  urlDemand: String = environment.apiUrl + 'demand';
  urlStatusSale: String = environment.apiUrl + 'statussales';
  // responseStatus: Boolean = false;
  // responseDemands: Boolean = false;
  page = 1;

  constructor(
    private http: HttpClient,
    private routes: Router
  ) {
  }

  // public async getListPedidosPorVendedor( pageNumber: number) {
  //   const idVendedor = 1;
  //   if (pageNumber > 0) {
  //     this.page = pageNumber;
  //   }
  //   this.http.get(this.urlDemand + '/bysalesman/' + idVendedor + '/' + this.page).subscribe(data => {
  //     // console.log(data);
  //     this.listDemands.emit(data);
  //     // this.responseDemands = true;
  //     // this.emitResponse();
  //   });
  //
  // }
  public updateStatusVenda(idDemand: number, status: number): any {

    this.http.put(this.urlDemand + '/update/' + idDemand + '/' + status, {}).subscribe(data => {
      this.updateStatus.emit(data);

    });
  }
  public getListStatusSale(): any {

    this.http.get(this.urlStatusSale + '/list').subscribe(data => {
      this.listStatusSale.emit(data);

    });
  }

  public getDemandByStatusSaleBySalesman(statusSalesID: number, page: number = 1): any {
    const salesmanId = localStorage.getItem('userid');
    // console.log('chamei a api');
    this.http.get(this.urlDemand + '/sale/' + statusSalesID + '/' + salesmanId + '/' + page).subscribe(data => {
      console.log(data);
      this.listDemands.emit(data);

    });
  }
  // public getDemandCompanyByName(keyword: string): any {
  //   const urlParams: string = this.urlDemand + '/listbyname/' + keyword;
  //   this.http.get(urlParams ).subscribe(data => {
  //     // this.columns.emit(Object.keys(data[0]));
  //     // this.brands.emit(data);
  //     console.log(data);
  //     this.companies.emit(data);
  //     this.responseStatus = true;
  //     this.responseDemands = true;
  //     this.emitResponse();
  //
  //   });
  // }

  // emitResponse() {
  //   this.allResponse.emit((this.responseDemands && this.responseStatus));
  // }
}
