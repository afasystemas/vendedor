import { TestBed, inject } from '@angular/core/testing';

import { MyOrderService } from './my-order.service';

describe('MyOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyOrderService]
    });
  });

  it('should be created', inject([MyOrderService], (service: MyOrderService) => {
    expect(service).toBeTruthy();
  }));
});
