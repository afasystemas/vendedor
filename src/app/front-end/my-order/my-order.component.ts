import {Component, OnDestroy, OnInit} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {dragula, DragulaService} from 'ng2-dragula';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Demand} from '../../entity/Demand';
import {OrderStatus} from '../../entity/OrderStatus';
import $ from 'jquery';
import {MyOrderService} from './my-order.service';
import {StatusSale} from '../../entity/StatusSale';
import {NewOpportunityComponent} from '../new-opportunity/new-opportunity.component';
import {LoggedService} from '../logged/logged.service';
import {environment} from '../../../environments/environment.prod';
import {Router} from '@angular/router';

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.css']
})
export class MyOrderComponent implements OnInit, OnDestroy {


  arrowChangeStatus = environment.apiUrlIcon + 'seta-icon.png';
  tabStatusActive: number;
  statusTarget: number; /*status que será mudado*/
  demandstatusChange: number;
  timerAlertOk: Boolean = false;
  timerAlertDanger: Boolean = false;

  ordersStatus: Array<OrderStatus> = [];
  // response: Boolean;
  containersDragula: any;
  arrayCompany: Array<any> = [];
  change: Boolean = false;
  idDragulaMove: String;
  companySearchName: String = '';
  showNewOpportunity: boolean ;

  oportunidade: Array<Demand> = [];
  contatoFeito: Array<Demand> = [];
  reuniao: Array<Demand> = [];
  negociacao: Array<Demand> = [];
  ganho: Array<Demand> = [];
  perdido: Array<Demand> = [];
  gerarPapelada: Array<Demand> = [];
  aguardandoAssinatura: Array<Demand> = [];
  contratoAssinado: Array<Demand> = [];
  uploadDoc: Array<Demand> = [];

  listStatus: Array<any> = [
    this.oportunidade,
    this.contatoFeito,
    this.reuniao,
    this.negociacao,
    this.ganho,
    this.perdido,
    this.gerarPapelada,
    this.aguardandoAssinatura,
    this.contratoAssinado,
    this.uploadDoc
  ];
  listStatusSale: Array<any>;
  listDemand: Array<any>;
  listOpportunity: Array<any>;
  pageOpportunity = 1;

  constructor (private myOrderService: MyOrderService,
              private modalService: NgbModal,
              private dragulaService: DragulaService,
               private routes: Router,
              private loggedService: LoggedService) {
    const self = this;
    this.showNewOpportunity = true;
    this.tabStatusActive = 0;
    this.listOpportunity = new Array<any>();

    // this.myOrderService.getListPedidosPorVendedor( 1);


    this.myOrderService.listStatusSale.subscribe((data) => {
      this.listStatusSale = data;
    });

    this.myOrderService.getDemandByStatusSaleBySalesman((this.tabStatusActive + 1));
    this.myOrderService.getListStatusSale();

    this.myOrderService.updateStatus.subscribe((data) => {console.log('updatestatus Subscribe');
      if (data.update) {
        this.myOrderService.getDemandByStatusSaleBySalesman((this.tabStatusActive + 1));
        this.timerAlertOk = true;
        setTimeout(() => {
            this.timerAlertOk = false;
          },
          5000);

      } else {
        this.timerAlertDanger = true;
        setTimeout(() => {
            this.timerAlertDanger = false;
          },
          5000);
      }
    });

    this.dragulaService.createGroup('PEDIDO', {
      revertOnSpill: true
    });

    dragulaService.cloned('PEDIDO').subscribe((value) => {
      this.idDragulaMove = value.clone.id;
      console.log(value.clone.id);
    });
    dragulaService.drag('PEDIDO').subscribe((value) => {
      console.log('ANTES');
      console.log(this.change);
      this.change = false;
    });

    this.myOrderService.listDemands.subscribe((value) => {

      this.targetStatus(this.tabStatusActive);
      const sizeData =  value.length;
      for (let c = 0; c < sizeData; c++) {
        this.listStatus[this.tabStatusActive].push(value[c]);
      }
      this.listDemand = [];
      this.listDemand = this.listStatus[this.tabStatusActive];
      console.log('adicionando a lista');
    });

    // dragulaService.drop('PEDIDO').subscribe((value) => {
    //   console.log('no dropModel');
    //   if (!this.change) {
    //     this.openFormModal();
    //     this.change = true;
    //   }
    // });
    this.dragulaService.shadow('PEDIDO').subscribe((value) => {
      console.log('aki no shadow');
    });

    // this.dragulaService.find('PEDIDO').drake.on('drop', (dropElem: any, target: any, source: any) => {
    //   const resp = confirm('Do you really want to erease me? Do you really want to wipe me out?!');
    //   console.log(resp);
    //
    //   console.log(dropElem);
    //   console.log(target);
    //   console.log(source);
    // });

    this.containersDragula = document.getElementsByClassName('container-dragula');
    // this.drake = dragula({
    //   copy: true,
    //   revertOnSpill: true,
    // }).on();
    // this.drake.containers.push(this.containersDragula);
    // this.drake.on('drop', function(el) { console.log('aki no cancela');
    //   console.log('aki deve cancelar');
    // }.bind(this));

    // document.getElementById('content-pedido');

    // this.dragulaService.dropModel('PEDIDO').subscribe(args => {
    //   console.log(args);
    //   // this.nameLast = args;
    //   console.log(' aki aki');
    //   // this.dragulaService.cancel('PEDIDO');
    // });
    this.dragulaService.find('PEDIDO').drake.on('drop', (el, container, source) => {
      console.log('teste');
      console.log(this.change);
      // this.dragulaService.find('PEDIDO').drake.cancel( false);

      if (!this.change) {
        // const resp = !confirm('Deseja mudar o status?'); /*async this.openFormModal()*/
        // this.dragulaService.find('PEDIDO').drake.cancel(resp);
        this.change = true;
        console.log(this.change);
        this.openFormModal();
      }
      console.log(this.change );
    });

  }

  ngOnInit() {
    // this.demandService.getListPedidosBKO(1);
    // this.demandService.getListStatusPedido();
    //
    // this.demandService.companies.subscribe(
    //   data => {console.log(data)
    //     this.demands = data;
    //     // this.reorderArray(data);
    //   }
    // );
    //
    // this.demandService.ordersStatus.subscribe(
    //   data => this.ordersStatus = data
    // );
    //
    // this.demandService.allResponse.subscribe(
    //   data => this.abbreviation(data)
    // );

  }

  activeItemLink(idElement: string, indexStatus: number) {
    $('#' + idElement).addClass('tab-active');
    $('.nav-link:not(#' + idElement + ')').removeClass('tab-active');
    (indexStatus === 0) ? this.showNewOpportunity = true : this.showNewOpportunity = false;

    this.targetStatus(indexStatus);
    this.tabStatusActive = indexStatus;
    // console.log('ActiveItemLink');

    if (this.listStatus[this.tabStatusActive].length === 0) {
      this.myOrderService.getDemandByStatusSaleBySalesman((indexStatus + 1));
    }

  }

  openFormModal () {
    // this.dragulaService.find('PEDIDO').drake.cancel( false);
    // const response = false;
    // const self = this;
    this.modalService.open(ModalComponent).result.then((result) => {
      // console.log(typeof result);
      // console.log(result);

      if (!result) {
        this.cancelar();
      } else {
        this.updateStatusLead();
      }
    }).catch((error) => {
      console.log(error);
    });

  }

  cancelar() {
    this.dragulaService.find('PEDIDO').drake.cancel(true);
  }

  onItemDrop(e: any) {

    // Get the dropped data here
    // console.log(e.dragData);
  }

  lookOrder() {
    // console.log(this.verificacaoDocumento);
    // console.log(this.demands);
    console.log(this.ordersStatus);
  }

  ngOnDestroy() {
    this.dragulaService.destroy('PEDIDO');
  }

  // reorderArray(data) {/*reorganiza em array cada empresa em seus respectivos status de pedido*/
  //   if (data) {
  //     this.arrayCompany = [];
  //     const self = this;
  //     this.ordersStatus.forEach(function (status, key) {
  //       const arrayCompanyStatus = [];
  //       self.demands.forEach(function (pedido, keyPedido) {
  //         if (status.id === pedido.OrderStatus.id) {
  //           arrayCompanyStatus.push(pedido.Company);
  //         }
  //       });
  //       self.arrayCompany.push(arrayCompanyStatus);
  //     });
  //   }
  //   console.log(this.arrayCompany);
  // }

  searchCompany() {
    console.log(this.companySearchName);
    // if (this.companySearchName !== '') {
    //   this.demandService.getDemandCompanyByName(this.companySearchName.toString());
    // } else {
    //   this.demandService.getListPedidosBKO(1);
    // }
  }

  // abbreviation(data) {
  //   if (data) {
  //     this.arrayCompany = [];
  //     const self = this;
  //     this.ordersStatus.forEach(function (status, key) {
  //       const arrayCompanyStatus = [];
  //       self.demands.forEach(function (pedidos, index) {
  //         for (let ind in pedidos) {
  //           /*abreviação de nomes grandes*/
  //           if (pedidos[ind].Company.razao_social.split(' ').length > 2) {
  //             const nameAll = pedidos[ind].Company.razao_social.split(' ');
  //             // console.log(nameAll);
  //             pedidos[ind].Company.razao_social = nameAll[0] + '. ' + nameAll[pedidos[ind].Company.razao_social.split(' ').length - 1];
  //             const remov = /\,/gi;
  //             pedidos[ind].Company.razao_social = pedidos[ind].Company.razao_social.replace(remov, '');
  //           }
  //           // console.log(pedidos[ind]);
  //           arrayCompanyStatus.push(pedidos[ind].Company);
  //         }
  //
  //         // self.demandStatus[key].push(pedidos.Company);
  //         // }
  //       });
  //       // console.log(self.demandStatus);
  //       self.demandStatus[key].push(arrayCompanyStatus);
  //     });

      // console.log(this.demands);
      // this.ordersStatus.forEach(function (status, key) {
      //   const arrayCompanyStatus = [];
      //
      //   self.demands.forEach(function (pedidoStatus, keyPedido) {
      //     const tam = pedidoStatus.length;
      //     for (let i = 0; i < tam; i++) {
      //       // console.log(pedidoStatus[i]);
      //       // console.log(pedidoStatus[i].Company.razao_social);
      //       if (pedidoStatus[i].Company.razao_social.length > 15) {
      //         // console.log(pedidoStatus[i].Company.razao_social);
      //         // console.log(pedidoStatus[i].Company.razao_social.split(' ', 2).toString());
      //         pedidoStatus[i].Company.razao_social = pedidoStatus[i].Company.razao_social.split(' ',  2 ).toString();
      //       }
      //       if (status.id === pedidoStatus[i].OrderStatus.id) {
      //         arrayCompanyStatus.push(pedidoStatus[i].Company);
      //       }
      //     }
      //     });
      //   self.arrayCompany.push(arrayCompanyStatus);
      // });
      // console.log(this.arrayCompany);
    // }
  // }
  // organizationList(data) {
  //   const self = this;
  //   data.forEach(function (val, key) {
  //     val.forEach(function (obj, ind) {
  //       self.listStatus[obj.StatusSale.id - 1].push(obj);
  //     });
  //   });
    // console.log(this.listStatus);
    // this.listDemand = [];
    // this.listStatus.forEach(function (obj, index) {
    //   // obj.forEach(function (dem, ind) {
    //   //   dem.forEach(function (v, i) {
    //       console.log(obj);
    //     // self.listDemand.push(v);
    //     // });
    //   // });
    // });
    // // this.listDemand = this.listStatus[this.tabStatusActive];
    // console.log(this.listDemand);

  // }

  targetStatus(indexStatus: number) {
    /*if para verificar que não há estouro de indice*/
    if (indexStatus < this.listStatus.length - 1 &&  indexStatus > -1) {
      this.listDemand = [];
      this.listDemand = this.listStatus[indexStatus];
      // console.log('targetStatus');
    }
  }
  changeStatusLead(targerStatusChange: number, idDemand: number) {
    this.statusTarget = targerStatusChange;
    this.demandstatusChange = idDemand;
    this.openFormModal();
  }
  updateStatusLead() {
    this.myOrderService.updateStatusVenda(this.demandstatusChange, this.statusTarget);
  }

  createOportunity() {
    this.modalService.open(NewOpportunityComponent).result.then((result) => {
      // console.log(typeof result);
      // console.log(result);

      if (!result) {
        this.cancelar();
      } else {
        // fazer chamada para api da lista de oportunidades buscando a
        // nova lista depois de ter acrescentado uma nova oportunidade
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  showHistoric(idSale) {
    if (idSale) {
      this.routes.navigate(['/logged/dashboard']);
    }
  }

  getMoreDemand() {
    this.pageOpportunity++;
    this.myOrderService.getDemandByStatusSaleBySalesman((this.tabStatusActive + 1), this.pageOpportunity);
  }
}
