import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment.prod';
import {User} from '../../entity/User';
import {LoginService} from './login.service';
import { Frases } from './Frases';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  public isCollapsed = true;
  user: User;
  timeAlert = false;
  token: any;
  urlLogoNetComm =  environment.apiUrlImage + 'logo-netcomm.png';
  urlLoginGoogle =  environment.apiUrlIcon + 'img/icons/common/google.svg';
  frases: string ;

  constructor( private router: Router, private serviceLogin: LoginService) {
    this.user = new User();
    this.token = null;
    const f = new Frases();
    this.frases = f.getOneFrase();

    this.serviceLogin.token.subscribe(data => { console.log('no token sub');
      this.token = data; console.log(data);
      if (this.token) {
        this.router.navigate(['/logged/dashboard']);
      } else {
        this.router.navigate(['']);
        this.timeAlert = true;
        setTimeout(() => {
            this.timeAlert = false;
          },
          5000);
      }
    });
  }



  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('bg-default');
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });

  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bg-default');
  }

  doLogin() {
    if (this.user.email !== '' && this.user.email !== undefined
      && this.user.password !== '' && this.user.password !== undefined) {
      this.serviceLogin.doLogin(this.user);
    } else {
      this.timeAlert = true;
      setTimeout(() => {
          this.timeAlert = false;
        },
        5000);
    }
  }

  utcTime(): void {
    setInterval(function() {
      this.myDate = new Date();
      console.log(this.myDate.getSeconds()); // just testing if it is working
    }, 1000);
  }

}
