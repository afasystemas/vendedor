import {EventEmitter, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';
import {User} from '../../entity/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  token: EventEmitter<any> = new EventEmitter<any>();
  user: EventEmitter<User> = new EventEmitter<User>();
  userLogged: User = new User();

  urlDemand: String = environment.apiUrl + 'login';

  constructor(private http: HttpClient, private routes: Router) {
    this.user.subscribe(data => {
      this.userLogged = data.UserAccess;

    });
  }

  public doLogin(user: User): any {
    const body = JSON.stringify(user);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = {
      headers: headers
    };
    this.http.post(this.urlDemand + '/do', body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
        .set('apikey', environment.KeySecurity),
    }).subscribe(data => {
      if (data) {
        // @ts-ignore
        const u = data.user;
        console.log(u);
        // @ts-ignore
        localStorage.setItem('token', data.token.toString());
        // @ts-ignore
        localStorage.setItem('userid', u.UserAccess.id);
        localStorage.setItem('userName', u.name);
        localStorage.setItem('userPhoto', u.UserAccess.urlphotoprofile);
        // @ts-ignore
        u.UserAccess.name =  data.name;

        //  this.userLogged = data.user;
        // @ts-ignore
        this.token.emit(data.token);
        // @ts-ignore
        // this.user.emit(data.user);
        u.UserAccess.name = u.name;
        // @ts-ignore
        this.user.emit(u.UserAccess);
        // @ts-ignore
        environment.firebase = data.firebase;
        // @ts-ignore
        environment.forRoot = data.forRoot;
        console.log(localStorage.getItem('userName'));


      } else {
        this.token.emit(null);
      }

    });
  }
}
