export class Frases {
   list: Array<string> = [
     '“A causa da derrota, não está nos obstáculos, ou no rigor das circunstâncias,' +
     ' está na falta de determinação e desistência da própria pessoa.”(Buda)',
     '“Daqui a vinte anos, você não vai se arrepender das coisas que fez, mas das ' +
     'que deixou de fazer. Por isso, veleje longe do seu porto seguro. Pegue os ventos. Explore.' +
     ' Sonhe. Descubra”(MarkTwain)',
     '“Para ser bem sucedido, o desejo pelo sucesso deve ser maior que o medo de falhar.”(Bill Cosby)',
     '“O sucesso não consiste em não errar, mas em não cometer os mesmos equívocos mais ' +
     'de uma vez”(George Bernad Shaw)',
     '“Inspiração vem dos outros. Motivação vem de dentro de nós.”',
     '“A maior glória não é ficar de pé, mas levantar-se cada vez que se cai.”(Confucio)',
     '“O mais importante ingrediente na fórmula do sucesso é saber como lidar ' +
     'com as pessoas.”(Theodore Roosevelt)',
     '“A lógica pode levar de um ponto A a um ponto B. A imaginação pode levar ' +
     'a qualquer lugar”(Albert Eistein)',
     '“As pessoas alcançam sucesso quando eles percebem que seus fracassos são ' +
     'uma preparação para suas vitórias.()Raph Waldo Emerson”',
     '“Não se preocupe com falhas, preocupe-se com as chances que você perde ' +
     'quando não tenta.”(Jack Canfield)'
   ];
   getOneFrase() {
    const min = 0;
    const max = 9;
    return this.list[Number.parseInt((Math.random() * (max - min) + min ).toString())];
  }
}

