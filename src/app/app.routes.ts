import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoggedComponent} from './front-end/logged/logged.component';
import {DashboardComponent} from './front-end/dashboard/dashboard.component';
import {AccountComponent} from './front-end/account/account.component';
import {MyDatasComponent} from './front-end/my-datas/my-datas.component';
import {MyDocumentsComponent} from './front-end/my-documents/my-documents.component';
import {BankAccountComponent} from './front-end/bank-account/bank-account.component';
import {NewOrderComponent} from './front-end/new-order/new-order.component';
import {ClientAddressComponent} from './front-end/client-address/client-address.component';
import {TypeProductComponent} from './front-end/type-product/type-product.component';
import {OperatorComponent} from './front-end/operator/operator.component';
import {MyOrderComponent} from './front-end/my-order/my-order.component';
import {LoginComponent} from './front-end/login/login.component';
import {TypeSaleComponent} from './front-end/type-sale/type-sale.component';
import {PlanComponent} from './front-end/plan/plan.component';
import {LinesComponent} from './front-end/lines/lines.component';
import {ServicesComponent} from './front-end/servicesSale/services.component';
import {SalesContractComponent} from './front-end/sales-contract/sales-contract.component';
import {DetailsSaleComponent} from './front-end/details-sale/details-sale.component';
import {ProposalComponent} from './front-end/proposal/proposal.component';
import {FinancialStatementComponent} from './front-end/financial-statement/financial-statement.component';
import {ActivityComponent} from './front-end/activity/activity.component';
import {NewActivityComponent} from './front-end/new-activity/new-activity.component';

@NgModule({
  exports: [ RouterModule ]
})
export class AppRoutesModule {}

export const routes: Routes = [
  { path: '', component: LoginComponent/*, outlet: 'home'*/},
  // { path: '', component: /*, outlet: 'home'*/},
  // { path: 'menu', component: MenuComponent/*, outlet: 'home'*/},
  // { path: 'funil-vendas', component: SalesFunnelComponent/*, outlet: 'home'*/},
  { path: 'logged', component: LoggedComponent/*, outlet: 'home'*/,
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'detales-pedido/:id', component: DetailsSaleComponent},
      {path: 'dados-venda', component: MyOrderComponent},
      {path: 'minhas-vendas', component: MyOrderComponent},
      {path: 'extrato-financeiro', component: FinancialStatementComponent},
      {path: 'historico-venda/:code', component: ActivityComponent},
      {path: 'novo-historico/:code/:statussale', component: NewActivityComponent},
      {path: 'nova-venda', component: NewOrderComponent,
        children: [
          {path: 'cliente', component: ClientAddressComponent},
          {path: 'tipo-venda', component: TypeSaleComponent},
          {path: 'planos', component: PlanComponent},
          {path: 'linhas', component: LinesComponent},
          {path: 'aparelhos', component: TypeProductComponent},
          {path: 'servicos', component: ServicesComponent},
          {path: 'papelada', component: SalesContractComponent},
          { path: 'proposta', component: ProposalComponent},
        ]
      },
      {path: 'minha-conta', component: AccountComponent,
        children: [
          {path: '', component: MyDatasComponent},
          {path: 'meus-documentos', component: MyDocumentsComponent},
          {path: 'conta-bancaria', component: BankAccountComponent}
        ]
      },
  //     {path: 'chat', component: ChatComponent},
  //     {path: 'check-documentation', component: CheckDocumentationComponent}
    ]
  },
  // { path: 'sign-up', component: SignUpComponent, outlet: 'login'},
  // { path: 'logged', component: PageLoggedComponent/*, canActivate: [AuthGuard], outlet: 'home'*/}
];
